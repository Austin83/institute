<?php
/**
 * Created by PhpStorm.
 * User: sebastianndiaz
 * Date: 23/03/19
 * Time: 11:44 AM
 */

require_once "bootstrap.php";

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($em);