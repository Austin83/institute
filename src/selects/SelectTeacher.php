<?php

abstract class SelectTeacher
{
    const id = 0;
    const document = 1;
    const username = 2;
    const name = 3;
    const surname = 4;
    const email = 5;
    const birthday = 6;
    const phones = 7;
    const registeredSince = 8;
    const subjects_possible = 9;
    const subjects_current = 10;
    const fullname = 11;
    const subjects = 12;
}