<?php

abstract class SelectStudent
{
    const id = 0;
    const document = 1;
    const username = 2;
    const name = 3;
    const surname = 4;
    const email = 5;
    const birthday = 6;
    const phones = 7;
    const registeredSince = 8;
}