<?php

abstract class SelectSubject
{
    const id = 0;
    const name = 1;
    const area = 2;
    const topics = 3;
    const priors = 4;
}