<?php

use \Slim\Http\Response;
use \Slim\Http\Request;

$app->get('/admins/users/unapproved', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $result = UsersController::getInstance()
        ->getUsersUnapproved();
    if( $result === HttpStatus::Bad_Request ){
        return $response->withStatus(HttpStatus::Bad_Request);
    }

    return $response->withJson($result,HttpStatus::OK,
        JSON_UNESCAPED_UNICODE);
});

$app->get('/admins/users', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $result = UsersController::getInstance()
        ->getUsers();
    if( $result === HttpStatus::Bad_Request ){
        return $response->withStatus(HttpStatus::Bad_Request);
    }

    return $response->withJson($result,HttpStatus::OK,
                                JSON_UNESCAPED_UNICODE);
});

$app->get('/admins/areas/exists', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $name =  $request->getQueryParam( "name" , $default = null);
    if( $name === null || $name === "" ){
        return $response->withStatus(HttpStatus::Bad_Request);
    }

    if( AreasController::getInstance()->exists($name) ){
        return $response->withStatus( HttpStatus::Conflict );
    }
    else{
        return $response->withStatus( HttpStatus::OK );
    }
});

$app->get('/admins/subjects/exists', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $name =  $request->getQueryParam( "name" , $default = null);
    if( $name === null || $name === "" ){
        return $response->withStatus(HttpStatus::Bad_Request);
    }

    if( SubjectsController::getInstance()->exists($name) ){
        return $response->withStatus( HttpStatus::Conflict );
    }
    else{
        return $response->withStatus( HttpStatus::OK );
    }
});

$app->get('/admins/classrooms/exists', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $name =  $request->getQueryParam( "name" , $default = null);
    if( $name === null || $name === "" ){
        return $response->withStatus(HttpStatus::Bad_Request);
    }

    if( ClassroomsController::getInstance()->exists($name) ){
        return $response->withStatus( HttpStatus::Conflict );
    }
    else{
        return $response->withStatus( HttpStatus::OK );
    }
});

$app->get('/admins/classrooms/free', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $params =  $request->getQueryParams();
    $valid = ClassroomsController::getInstance()->ValidateFree($params);

    if( $valid === HttpStatus::OK ){
        $status = ClassroomsController::getInstance()->getFreeClassrooms($params);
        if( $status === HttpStatus::Not_Found ){
            return $response->withStatus(HttpStatus::Not_Found);
        }else{
            return $response->withJson($status, HttpStatus::OK);
        }
    }
    else{
        return $response->withStatus( $valid );
    }
});

$app->patch('/admins/students', function (Request $request, Response $response) use ($app) {

    if(! TokenHelper::getInstance()->grantAdminAccess($request
        ->getHeader('authorization')) ){
        $data = new StdClass();
        $data->error = "Access denied: you are not an admin !";
        $result = array( 'success' => 'false', 'data' => $data );
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    $json = $request->getBody();
    $data = json_decode($json, true)['data'];

    $filter = array( 'document' => $data['target_document'] );
    $student = CollectionHelper::getInstance()->findByFilter('Student', $filter);
    $student = $student[0];
    if( $student === null ){
        $data = new StdClass();
        $data->error = "Student was not found with document= ".$filter['document']." !";
        $result = array('success' => 'false', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( ! $student->getApproved() ){
        $data = new StdClass();
        $data->error = "Student is not approved!";
        $result = array('success' => 'false', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( StudentsController::getInstance()->validateUpdateByAdmin($data) ){
        StudentsController::getInstance()->updateByAdmin($data);
        $result = array('success' => 'true', 'data' => $data);
    } else{
        $result = array('success' => 'false', 'data' => $data);
    }

    return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
});

$app->patch('/admins/admissions', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $state = json_decode($request->getBody(), true);

    $status = AdminsController::getInstance()->validateAdmissionSwitch($state);
    if( $status === HttpStatus::OK ) {
        AdminsController::getInstance()->admissions_switch($state);
        return $response->withStatus(HttpStatus::OK);
    }
    else{
        return $response->withStatus($status);
    }
});

$app->patch('/admins/teachers', function (Request $request, Response $response) use ($app) {

    if(! TokenHelper::getInstance()->grantAdminAccess($request
        ->getHeader('authorization')) ){
        $data = new StdClass();
        $data->error = "Access denied: you are not an admin !";
        $result = array( 'success' => 'false', 'data' => $data );
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    $json = $request->getBody();
    $data = json_decode($json, true)['data'];

    $filter = array( 'document' => $data['target_document'] );
    $teacher = CollectionHelper::getInstance()->findByFilter('Teacher', $filter);
    $teacher = $teacher[0];
    if( $teacher === null ){
        $data = new StdClass();
        $data->error = "Teacher was not found with document= ".$filter['document']." !";
        $result = array('success' => 'true', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( ! $teacher->getApproved() ){
        $data = new StdClass();
        $data->error = "Teacher is not approved!";
        $result = array('success' => 'true', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( TeachersController::getInstance()->validateUpdateByAdmin($data) ){
        TeachersController::getInstance()->updateByAdmin($data);
        $result = array('success' => 'true', 'data' => $data);
    } else{
        $result = array('success' => 'false', 'data' => $data);
    }

    return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
});

$app->post('/admins/classrooms', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);

    $status = ClassroomsController::getInstance()->ValidateRegister($data);

    if( $status === HttpStatus::OK  ){
        return $response->withJson(ClassroomsController::getInstance()
            ->create($data), HttpStatus::OK);
    } else{
        return $response->withStatus($status);
    }
});

$app->post('/admins/areas', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);

    $status = AreasController::getInstance()->validateRegister($data);
    if( $status === HttpStatus::OK ){
        $area = AreasController::getInstance()->create($data);
        return $response->withJson( array('id_area' => $area),
            $status, JSON_UNESCAPED_UNICODE);
    }
    else{
        return $response->withStatus($status);
    }
});

$app->patch('/admins/areas', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);

    $status = AreasController::getInstance()->validateUpdate($data);
    if( $status === HttpStatus::OK ){
        AreasController::getInstance()->update($data);
        return $response->withStatus(HttpStatus::OK);
    }
    else{
        return $response->withStatus($status);
    }
});

$app->patch('/admins/classrooms', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);

    $status = ClassroomsController::getInstance()->validateUpdate($data);
    if( $status === HttpStatus::OK ){
        ClassroomsController::getInstance()->update($data);
        return $response->withStatus(HttpStatus::OK);
    }
    else{
        return $response->withStatus($status);
    }
});

$app->patch('/admins/subjects', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);
    $status = SubjectsController::getInstance()->validateUpdate( $data);

    if( $status === HttpStatus::OK ){
        SubjectsController::getInstance()->update( $data );
        return $response->withStatus($status);

    } else{
        return $response->withStatus($status);
    }
});

$app->post('/admins/subjects', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);

    if( SubjectsController::getInstance()
            ->validateRegister( $data) === HttpStatus::OK ){
        return $response->withJson(SubjectsController::getInstance()
            ->create( $data ),HttpStatus::OK);

    } else{
        return $response->withStatus(HttpStatus::Bad_Request);
    }
});

$app->post('/admins/lessons', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);
    $status = LessonsController::getInstance()->validateRegister( $data);

    if( $status === HttpStatus::OK ){
        LessonsController::getInstance()->createBatch( $data );
        return $response->withStatus($status);
    } else{
        return $response->withStatus($status);
    }
});

$app->post('/admins/topics', function (Request $request, Response $response) use ($app) {

    if(! TokenHelper::getInstance()->grantAdminAccess($request
        ->getHeader('authorization')) ){
        $data = new StdClass();
        $data->error = "Access denied!";
        $result = array( 'success' => 'false', 'data' => $data );
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    $json = $request->getBody();
    $data = json_decode($json, true)['data'];

    $syllabus = null;
    if( $data['syllabus_id'] !== null ){
        $syllabus = CollectionHelper::getInstance()->find("Syllabus",$data['syllabus_id']);
        if( $syllabus === null ){
            LogHelper::getInstance()->log
            ("Register Topic: syllabus not found with id= ".$data['syllabus_id'] );
        }
    }
    else{
        LogHelper::getInstance()->log
        ("Register Topic: 'syllabus_id' is null");
    }

    if( $syllabus !== null && TopicsController::getInstance()->validateRegister($data,$syllabus) ){
        $topic = TopicsController::getInstance()->create($data, $syllabus);
        $parent = null;

        if( $data['parent_id'] !== null ){
            $parent = CollectionHelper::getInstance()->find("Topic",$data['parent_id']);
            $parent->addChildren( $topic );
            $GLOBALS['em']->persist($parent);
        }

        $GLOBALS['em']->flush();

        $result = array('success' => 'true', 'data' => $data);
    } else{
        $result = array('success' => 'false', 'data' => $data);
    }

    return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
});

$app->patch('/admins/topics', function (Request $request, Response $response) use ($app) {

    if(! TokenHelper::getInstance()->grantAdminAccess($request
        ->getHeader('authorization')) ){
        $data = new StdClass();
        $data->error = "Access denied!";
        $result = array( 'success' => 'false', 'data' => $data );
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    $json = $request->getBody();
    $data = json_decode($json, true)['data'];

    $syllabus = CollectionHelper::getInstance()->find("Syllabus", $data["syllabus_id"]);
    $filter = Array( "name" => $data['target_name'] , "syllabus" => $syllabus);
    $topic = CollectionHelper::getInstance()->findByFilter("Topic", $filter);
    $topic = $topic[0];

    if( $topic !== null && TopicsController::getInstance()->validateUpdate($data, $syllabus)){
        TopicsController::getInstance()->update( $data, $topic );
        $result = array('success' => 'true', 'data' => $data);
    } else{
        $result = array('success' => 'false', 'data' => $data);
    }

    // Return response, with an 200 (OK) status, and Unicode encoding
    return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
});

$app->patch('/admins/users/approve', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantAdminAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $data = json_decode($request->getBody(), true);
    $status = UsersController::getInstance()->validateApprove($data);
    if( $status === HttpStatus::OK ){
        UsersController::getInstance()->approve($data);
    }
    return $response->withStatus($status);
});

$app->get('/admins/about', function () use ($app) {
    return json_encode(
        array(
            'about' => 'Servicios para administradores de sistema.'
        )
        , JSON_UNESCAPED_UNICODE);
});

// Si hay problemas con los tildes, yo lo resolví con esta página:
// https://stackoverflow.com/questions/4076988/php-json-encode-json-decode-utf-8#30424859