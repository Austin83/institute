<?php

use \Slim\Http\Response;
use \Slim\Http\Request;

$app->get('/admissions', function (Request $request, Response $response) use ($app) {

    return $response->withJson(AdminsController::getInstance()->getAdmissions(),
        HttpStatus::OK);
});

$app->get('/users/exists', function (Request $request, Response $response) use ($app) {

    $identifier =  $request->getQueryParam( "id" , $default = null);
    if( $identifier === null || $identifier === "" || $identifier === "@"){
        LogHelper::getInstance()->log("User Exits: null parameter");
        return $response->withStatus(HttpStatus::Bad_Request);
    }

    if( UsersController::getInstance()->exists($identifier) ){
        return $response->withStatus( HttpStatus::Conflict );
    }
    else{
        return $response->withStatus( HttpStatus::OK );
    }
});

$app->post('/teachers', function (Request $request, Response $response) use ($app) {

    $data = json_decode($request->getBody(), true);
    $status = TeachersController::getInstance()->validateRegister( $data );

    if( $status === HttpStatus::OK){
        TeachersController::getInstance()->create($data);
        return $response->withStatus(HttpStatus::OK);

    } else{
        return $response->withStatus($status);
    }
});

$app->get('/subjects', function (Request $request,Response $response) use ($app) {

    $filter_subject = new InfoHelper( array(), GetInfoModes::Pesimistic );

    $filter_topic = new InfoHelper( array(
        SelectTopic::subject_id
    ), GetInfoModes::Pesimistic );

    $fiter_area = new InfoHelper( array(
        SelectArea::id,
        SelectArea::name
    ), GetInfoModes::Optimistic);

    $result = SubjectsController::getInstance()
        ->getSubjects($filter_subject, $filter_topic, $fiter_area);

    if( $result === HttpStatus::Not_Found ){
        return $response->withStatus($result);
    }

    return $response->withJson( $result,HttpStatus::OK,
                                JSON_UNESCAPED_UNICODE);
});

$app->get('/areas', function (Request $request,Response $response) use ($app) {

    $filter_subject = new InfoHelper( array(
        SelectArea::id,
        SelectArea::name,
        SelectArea::teacher_id
    ), GetInfoModes::Optimistic );

    $result = AreasController::getInstance()->getAreas($filter_subject);

    if( $result === HttpStatus::Not_Found ){
        return $response->withStatus($result);
    }

    return $response->withJson( $result,HttpStatus::OK,
        JSON_UNESCAPED_UNICODE);
});

$app->post('/students', function (Request $request, Response $response) use ($app) {

    $data = json_decode($request->getBody(), true);
    $status = StudentsController::getInstance()->validateRegister($data);

    if( $status === HttpStatus::OK ){
        StudentsController::getInstance()->create($data);
    }

    return $response->withStatus($status);
});

$app->post('/login', function (Request $request, Response $response) use ($app) {

    $data = json_decode($request->getBody(), true);

    $login = UsersController::getInstance()->login($data);

    if( $login === HttpStatus::Unauthorized ){
        return $response->withStatus($login);
    } else{
        return $response
            ->withJson($login, HttpStatus::OK,
                JSON_UNESCAPED_UNICODE);
    }
});

$app->get('/about', function () use ($app) {
    return json_encode(
        array(
            'about' => 'Un about bien detallado y extenso.',
            'entero' => 5,
            'prueba de tildes' => 'relámpago'
        )
        , JSON_UNESCAPED_UNICODE);
});

// Si hay problemas con los tildes, yo lo resolví con esta página:
// https://stackoverflow.com/questions/4076988/php-json-encode-json-decode-utf-8#30424859