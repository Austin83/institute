<?php

use \Slim\Http\Response;
use \Slim\Http\Request;

$app->get('/students', function (Request $request, Response $response) use ($app) {

    $result = TokenHelper::getInstance()->grantUserAccess( $request , $response);
    if( $result !== null ){
        return $result;
    }

    $json = $request->getBody();
    $data = json_decode($json, true);

    if( $data === null ){
        $students = CollectionHelper::getInstance()->findAll("Student");
        $result = array('success' => 'true', 'data' => $students );
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }
    if( $data['filter'] !== null ){
        foreach ( $data['filter'] as $key => $value ){
            if( $value === null ){
                $result = array('success' => 'false', 'data' => null);
                return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
            }
        }
        $students = CollectionHelper::getInstance()
            ->findByFilter( "Student" ,$data['filter']);
    }else{
        $students = CollectionHelper::getInstance()
            ->findByFilter( "Student" , null);
    }

    if( $students === null ){
        $result = array('success' => 'false', 'data' => null);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( empty( $students ) ){
        $result = array('success' => 'true', 'data' => array());
    } else{
        if( $data['select'] !== null && !empty($data['select']) ){
            $students = CollectionHelper::getInstance()
                ->select( $students, $data['select'] );
        }
        $result = array('success' => 'true', 'data' => $students );
    }

    // Return response, with an 200 (OK) status, and Unicode encoding
    return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
});

$app->get('/teachers', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantBasicAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $filter_teacher = new InfoHelper( array(
        SelectTeacher::id,
        SelectTeacher::fullname,
        SelectTeacher::subjects
    ), GetInfoModes::Optimistic );

    $result = TeachersController::getInstance()->getTeachers($filter_teacher);

    if(  $result === HttpStatus::Not_Found){
        return $response->withStatus(HttpStatus::Not_Found);
    }

    return $response->withJson($result,HttpStatus::OK, JSON_UNESCAPED_UNICODE);
});

$app->get('/lessons', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantBasicAccess( $request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    return $response->withJson(LessonsController::getInstance()
        ->getLessons($request->getHeader('authorization')),
        HttpStatus::OK, JSON_UNESCAPED_UNICODE);
});

$app->get('/subjects/enrolled', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantStudentAccess( $request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $status =  StudentsController::getInstance()
        ->getSubjectsEnrolled($request->getHeader('authorization'));

    if( $status === HttpStatus::Not_Found){
        return $response->withStatus(HttpStatus::Not_Found);
    }

    return $response->withJson($status,
        HttpStatus::OK, JSON_UNESCAPED_UNICODE);
});

$app->patch('/teachers', function (Request $request, Response $response) use ($app) {

    $json = $request->getBody();
    $data = json_decode($json, true);

    $id = TokenHelper::getInstance()->getUserId($request->getHeader('authorization'));
    $filter = array( 'id' => $id );
    $teacher = CollectionHelper::getInstance()->findByFilter('Teacher', $filter);
    $teacher = $teacher[0];
    if( $teacher === null ){
        $data = new StdClass();
        $data->error = "Teacher was not found with id= ".$id." !";
        $result = array('success' => 'true', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( ! $teacher->getApproved() ){
        $data = new StdClass();
        $data->error = "Teacher is not approved!";
        $result = array('success' => 'true', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if(! TokenHelper::getInstance()->grantUserAccess($request
        ->getHeader('authorization'), $teacher->getUsername()) ){
        $data = new StdClass();
        $data->error = "Access denied!";
        $result = array( 'success' => 'false', 'data' => $data );
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( TeachersController::getInstance()->validateUpdateByTeacher($data, $teacher->getDocument()) ){
        TeachersController::getInstance()->updateByTeacher($data, $teacher->getDocument());
        $result = array('success' => 'true', 'data' => $data);
    } else{
        $result = array('success' => 'false', 'data' => $data);
    }

    return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
});

$app->patch('/students', function (Request $request, Response $response) use ($app) {

    $json = $request->getBody();
    $data = json_decode($json, true);

    $id = TokenHelper::getInstance()->getUserId($request->getHeader('authorization'));
    $filter = array( 'id' => $id );
    $student = CollectionHelper::getInstance()->findByFilter('Student', $filter);
    $student = $student[0];
    if( $student === null ){
        $data = new StdClass();
        $data->error = "Student was not found with id= ".$id." !";
        $result = array('success' => 'true', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( ! $student->getApproved() ){
        $data = new StdClass();
        $data->error = "Student is not approved!";
        $result = array('success' => 'true', 'data' => $data);
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if(! TokenHelper::getInstance()->grantUserAccess($request
        ->getHeader('authorization'), $student->getUsername()) ){
        $data = new StdClass();
        $data->error = "Access denied!";
        $result = array( 'success' => 'false', 'data' => $data );
        return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
    }

    if( StudentsController::getInstance()->validateUpdateByStudent($data, $student->getDocument()) ){
        StudentsController::getInstance()->updateByStudent($data, $student->getDocument());
        $result = array('success' => 'true', 'data' => $data);
    } else{
        $result = array('success' => 'false', 'data' => $data);
    }

    return $response->withJson($result, 200, JSON_UNESCAPED_UNICODE);
});

$app->post('/lessons/enroll', function ( Request $request ,Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantStudentAccess( $request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $status =  AttendancesController::getInstance()->validateEnrollMany(
        $request->getHeader('authorization'),
        json_decode($request->getBody(), true)
    );

    if( $status === HttpStatus::OK ){
        AttendancesController::getInstance()
            ->enrollMany($request->getHeader('authorization'),
                        json_decode($request->getBody(), true));

        return $response->withStatus(HttpStatus::OK);
    } else{
        return $response->withStatus($status);
    }
});

$app->post('/subjects/enroll', function ( Request $request ,Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantStudentAccess( $request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $status =  InscriptionsController::getInstance()->validateEnroll(
        $request->getHeader('authorization'),
        json_decode($request->getBody(), true)
    );

    if( $status === HttpStatus::OK ){
        return $response->withJson(
            InscriptionsController::getInstance()->enroll(
                $request->getHeader('authorization'),
                json_decode($request->getBody(), true)
            ),
            HttpStatus::OK, JSON_UNESCAPED_UNICODE);
    } else{
        return $response->withStatus($status);
    }
});

$app->get('/users/about', function () use ($app) {
    return json_encode(
        array(
            'about' => 'Servicios para Estudiantes y Profesores registrados.'
        )
        , JSON_UNESCAPED_UNICODE);
});

$app->get('/classrooms', function (Request $request, Response $response) use ($app) {

    $security = TokenHelper::getInstance()
        ->grantBasicAccess($request->getHeader('authorization'));
    if( $security === HttpStatus::Unauthorized ){
        return $response->withStatus(HttpStatus::Unauthorized);
    }

    $result = ClassroomsController::getInstance()->getClassrooms();

    if(  $result === HttpStatus::Not_Found){
        return $response->withStatus(HttpStatus::Not_Found);
    }

    return $response->withJson($result,HttpStatus::OK, JSON_UNESCAPED_UNICODE);
});

// Si hay problemas con los tildes, yo lo resolví con esta página:
// https://stackoverflow.com/questions/4076988/php-json-encode-json-decode-utf-8#30424859