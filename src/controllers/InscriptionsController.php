<?php


class InscriptionsController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new InscriptionsController();
        }
        return self::$instance;
    }

    public function validateEnroll( array $authHeader, array $data ) : int {

        if( TokenHelper::getInstance()->evaluateAddmissions() === HttpStatus::Not_Acceptable ){
            return HttpStatus::Not_Acceptable;
        }

        $user = TokenHelper::getInstance()->getUser($authHeader);
        if( $user === null ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: user not found ! Who are you ?");
            return HttpStatus::Unauthorized;
        }

        if( ! $user->getApproved() ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: user is not approved !");
            return HttpStatus::Unauthorized;
        }

        if( ! is_array($data) || empty($data) ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: no data found !");
            return HttpStatus::Bad_Request;
        }

        if( ! array_key_exists("id_subject", $data) ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: key id_subject is missing !");
            return HttpStatus::Bad_Request;
        }

        if( $data["id_subject"] === null ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: id_subject is null !");
            return HttpStatus::Bad_Request;
        }

        if( empty($data["id_subject"]) ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: id_subject is emtpy !");
            return HttpStatus::Bad_Request;
        }
        $id_subject = (int)$data["id_subject"];

        if( ! is_int($id_subject) ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: id_subject is not int !");
            return HttpStatus::Bad_Request;
        }

        $subject = CollectionHelper::getInstance()->find("Subject", $id_subject);

        if( $subject === null ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: subject ".
                    $id_subject." was not found !");
            return HttpStatus::Bad_Request;
        }
        $filter = array(
            "student" => $user,
            "subject" => $subject
        );
        $inscriptions = CollectionHelper::getInstance()
            ->findByFilter("Inscription", $filter);
        if( is_array($inscriptions) || ! empty( $inscriptions )  ){
            LogHelper::getInstance()->
            log("Enroll to Subject: student ".$user->getId()
                ." is already enrolled in subject ".$id_subject);
            return HttpStatus::Conflict;
        }

        return HttpStatus::OK;
    }

    public function validateEnrollTest( int $id_student, int $id_subject ) : int {

        $user = CollectionHelper::getInstance()->find("Student",$id_student);
        if( $user === null ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: user ".$id_student." was not found !");
            return HttpStatus::Bad_Request;
        }
        $subject = CollectionHelper::getInstance()
            ->find("Subject", $id_subject);

        if( $subject === null ){
            LogHelper::getInstance()
                ->log("Enroll to Subject: subject ".
                    $id_subject." was not found !");
            return HttpStatus::Bad_Request;
        }
        $filter = array(
            "student" => $user,
            "subject" => $subject
        );
        $inscriptions = CollectionHelper::getInstance()
            ->findByFilter("Inscription", $filter);
        if( is_array($inscriptions) || ! empty( $inscriptions )  ){
            LogHelper::getInstance()->
            log("Enroll to Subject: student ".$user->getId()
                ." is already enrolled in subject ".$id_subject);
            return HttpStatus::Conflict;
        }

        return HttpStatus::OK;
    }

    public function enroll( array $authHeader, array $data ){
        $inscription = new Inscription(
            TokenHelper::getInstance()->getUser($authHeader),
            CollectionHelper::getInstance()->find("Subject",
                $data["id_subject"]));

        return $inscription->getId();
    }

    public function enrollTest( Student $student, Subject $subject ){
        $inscription = new Inscription($student,$subject);
        return $inscription->getId();
    }
}