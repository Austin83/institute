<?php

class AttendancesController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new AttendancesController();
        }
        return self::$instance;
    }

    public function validateEnroll( array $authHeader, array $data ) : int {

        $user = TokenHelper::getInstance()->getUser($authHeader);
        if( $user === null ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: user not found ! Who are you ?");
            return HttpStatus::Unauthorized;
        }

        if( ! $user->getApproved() ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: user is not approved !");
            return HttpStatus::Unauthorized;
        }

        if( ! is_array($data) || empty($data) ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: no data found !");
            return HttpStatus::Bad_Request;
        }

        if( ! array_key_exists("id_lesson", $data) ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: key id_lesson is missing !");
            return HttpStatus::Bad_Request;
        }

        if( $data["id_lesson"] === null ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: id_lesson is null !");
            return HttpStatus::Bad_Request;
        }

        if( empty($data["id_lesson"]) ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: id_lesson is emtpy !");
            return HttpStatus::Bad_Request;
        }
        $id_lesson = (int)$data["id_lesson"];

        if( ! is_int($id_lesson) ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: id_lesson is not int !");
            return HttpStatus::Bad_Request;
        }

        if( CollectionHelper::getInstance()
                ->find("Lesson", $id_lesson) === null ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: lesson ".
                    $id_lesson." was not found !");
            return HttpStatus::Bad_Request;
        }

        $filter = array(
            "student" => $user,
            "lesson"
            => CollectionHelper::getInstance()->find("Lesson", $id_lesson )
        );
        $attendances = CollectionHelper::getInstance()
            ->findByFilter("Attendance", $filter);
        if( is_array($attendances) || ! empty( $attendances )  ){
            LogHelper::getInstance()->
            log("Enroll to Lesson: student ".$user->getId()
                ." is already enrolled in lesson ".$id_lesson);
            return HttpStatus::Conflict;
        }

        return HttpStatus::OK;
    }

    public function validateEnrollMany( array $authHeader, array $data ) : int {

        $user = TokenHelper::getInstance()->getUser($authHeader);
        if( $user === null ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: user not found ! Who are you ?");
            return HttpStatus::Unauthorized;
        }

        if( ! $user->getApproved() ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: user is not approved !");
            return HttpStatus::Unauthorized;
        }

        if( ! is_array($data) || empty($data) ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: no data found !");
            return HttpStatus::Bad_Request;
        }

        foreach ($data as $node){

                if( ! is_int($node) ){
                    LogHelper::getInstance()
                        ->log("Enroll to Lesson: some lesson id is not int !");
                    return HttpStatus::Bad_Request;
                }

                $lesson_aux = CollectionHelper::getInstance()
                    ->find("Lesson", $node);
                if( $lesson_aux === null ){
                    LogHelper::getInstance()
                        ->log("Enroll to Lesson: lesson ".
                            $node." was not found !");
                    return HttpStatus::Bad_Request;
                }

                $filter = array(
                    "student" => $user,
                    "lesson" => $lesson_aux
                );
                $attendances = CollectionHelper::getInstance()
                    ->findByFilter("Attendance", $filter);
                if( is_array($attendances) || ! empty( $attendances )  ){
                    LogHelper::getInstance()->
                    log("Enroll to Lesson: student ".$user->getId()
                        ." is already enrolled in lesson ".$node);
                    return HttpStatus::Conflict;
                }
        }

        return HttpStatus::OK;
    }

    public function validateEnrollTest( int $id_student, int $id_lesson ) : int {

        if( $id_student === null){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: id_student is null !");
            return HttpStatus::Bad_Request;
        }

        if( ! is_int($id_student) ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: invalid id_student= ".$id_student." !");
            return HttpStatus::Bad_Request;
        }

        $user = CollectionHelper::getInstance()->find("Student",$id_student);
        if( $user === null ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: user ".
                    $id_student." found ! Who are you ?");
            return HttpStatus::Unauthorized;
        }

        if( ! $user->getApproved() ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: user is not approved !");
            return HttpStatus::Unauthorized;
        }

        if( $id_lesson === null ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: id_lesson is null !");
            return HttpStatus::Bad_Request;
        }

        if( ! is_int($id_lesson) ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: invalid id_lesson= ".$id_lesson." !");
            return HttpStatus::Bad_Request;
        }

        $lesson = CollectionHelper::getInstance()
            ->find("Lesson",$id_lesson);

        if( $lesson === null ){
            LogHelper::getInstance()
                ->log("Enroll to Lesson: lesson ".
                    $id_lesson." was not found !");
            return HttpStatus::Bad_Request;
        }
        $filter = array(
            "student" => $user,
            "lesson" => $lesson
        );
        $attendances = CollectionHelper::getInstance()
            ->findByFilter("Attendance", $filter);
        if( is_array($attendances) || ! empty( $attendances )  ){
            LogHelper::getInstance()->
            log("Enroll to Lesson: student ".$user->getId()
                ." is already enrolled in lesson ".$id_lesson);
            return HttpStatus::Conflict;
        }

        return HttpStatus::OK;
    }

    public function enroll( array $authHeader, array $data ){
        $attendance = new Attendance(
            TokenHelper::getInstance()->getUser($authHeader),
            $data["id_lesson"]);

        return $attendance->getId();
    }

    public function enrollMany( array $authHeader, array $data ){
        $user = TokenHelper::getInstance()->getUser($authHeader);
        foreach ($data as $id){
            new Attendance($user,$id);
        }
    }

    public function enrollTest( Student $student, int $id_lesson ){
        $attendance = new Attendance($student,$id_lesson);
        return $attendance->getId();
    }
}