<?php

class UsersController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new UsersController();
        }
        return self::$instance;
    }

    public function existsByFilter(array $filter, string $whitelist) : bool
    {
        $arr = $GLOBALS['em']->getRepository('User')->findBy($filter);

        $size = count( $arr );
        if( $size === 1 ){
            $arr = $arr[0];
            if( $whitelist !== null ){
                if( $arr->getDocument() === $whitelist ){
                    return false;
                }
            }
            return (bool)$arr;
        }

        if( empty($arr) ){
            return false;
        }

        if( $size > 1 ){
            return true;
        }
        return (bool)$arr;
    }

    public function login(array $credentials)
    {
        /*if( $credentials['type'] === UserCredentials::Username ){
            if( $credentials['identifier'][0] === '@' ){
                $credentials['identifier'] = substr($credentials['identifier'], 1);
            }
        }*/

        $arr = Array( $credentials['type'] => $credentials['identifier'] );
        $user = CollectionHelper::getInstance()->findByFilter("User", $arr );
        if( (!empty($user)) && count($user) !== 1 || ( $user[0] === null) ){
            return HttpStatus::Unauthorized;
        }
        $user = $user[0];
        if( ! $user->getApproved() ){
            return HttpStatus::Unauthorized;
        }

        if( ! password_verify( $credentials['password'], $user->getPassword()) ){
            return HttpStatus::Unauthorized;
        }
        else{
            $arr = Array(
                "id" => $user->getId(),
                "username" => $user->getUsername(),
                "type" => $user->getType());
            $token = TokenHelper::getInstance()->create(true, $arr);
            $result = array_merge($arr, array( "token" => $token));
            return $result;
        }
    }

    public function validateApprove(array $data) : int
    {
        if( empty($data) ){
            LogHelper::getInstance()
                ->log("Approve User: data is empty !");
            return HttpStatus::Bad_Request;
        }
        if( array_key_exists("state", $data)){
            if( ! is_bool($data["state"]) ){
                LogHelper::getInstance()
                    ->log("Approve User: state is not bool !");
                return HttpStatus::Bad_Request;
            }
        }else{
            LogHelper::getInstance()
                ->log("Approve User: state key is missing !");
            return HttpStatus::Bad_Request;
        }

        if( array_key_exists("ids", $data)){
            if( is_array($data["ids"]) ){
                if( empty($data["ids"]) ){
                    LogHelper::getInstance()
                        ->log("Approve User: ids is empty !");
                    return HttpStatus::Bad_Request;
                }
            }else{
                LogHelper::getInstance()
                    ->log("Approve User: ids is not array !");
                return HttpStatus::Bad_Request;
            }
        }else{
            LogHelper::getInstance()
                ->log("Approve User: ids key is missing !");
            return HttpStatus::Bad_Request;
        }
        return HttpStatus::OK;
    }

    public function approve(array $data)
    {
        foreach( $data["ids"] as $id ) {
            $user = CollectionHelper::getInstance()
                ->find("User", $id);
            if ($user !== null) {
                if ($user->getApproved() === false && $data["state"] === false) {
                    $GLOBALS["em"]->remove($user);
                }
                if ($user->getApproved() === false && $data["state"] === true) {
                    $user->setApproved($data["state"]);
                    $GLOBALS["em"]->persist($user);
                }
            } else {
                LogHelper::getInstance()
                    ->log("Approve Users: does not exists User with id= " .
                        $id . " !");
            }
        }
        $GLOBALS["em"]->flush();
    }

    public function exists( string $identifier ) : bool
    {
        $id_type = null;

        preg_match_all("/[\d]+/", $identifier, $coincidencias );
        if( strlen($identifier) === DataConstants::Document
            && $coincidencias[0][0] === $identifier ){
            $id_type = UserCredentials::Document;
        }

        if( $identifier[0] === '@' ){
            $identifier = substr($identifier, 1);
            preg_match_all("/[\w\.\-\_]+/", $identifier, $coincidencias );
            if( strlen($identifier) === DataConstants::Username
                && $coincidencias[0][0] === $identifier ){
                $id_type = UserCredentials::Username;
            }
        }
        else{
            if (filter_var($identifier, FILTER_VALIDATE_EMAIL)) {
                $id_type = UserCredentials::Email;
            }
        }

        $filter = null;

        switch ($id_type){
            case UserCredentials::Document:
                $filter = array( "document" => $identifier );
                break;
            case UserCredentials::Username:
                $filter = array( "username" => $identifier );
                break;
            case UserCredentials::Email:
                $filter = array("email" => $identifier);
                break;
        }
        return CollectionHelper::getInstance()
            ->existsByFilter("User", $filter);
    }

    public function getUsers(){
        $entities = CollectionHelper::getInstance()->findAll("User");
        if( empty($entities) ){
            return HttpStatus::Not_Found;
        }
        $result = array();
        foreach( $entities as $user ){
            array_push($result, $user->getInfoAll());
        }
        return $result;
    }

    public function getUsersUnapproved(){
        $filter = array ( "approved" => false );
        $entities = CollectionHelper::getInstance()
            ->findByFilter("User", $filter);
        if( empty($entities) ){
            return HttpStatus::Not_Found;
        }
        $result = array();
        foreach( $entities as $user ){
            array_push($result, $user->getInfoAll());
        }
        return $result;
    }
}