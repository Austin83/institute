<?php

class StudentsController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new StudentsController();
        }
        return self::$instance;
    }

    public function validateRegister( array $data ) : int
    {
        if( TokenHelper::getInstance()->evaluateAddmissions() === HttpStatus::Not_Acceptable ){
            return HttpStatus::Not_Acceptable;
        }

        $filter = array("name" => DataConstants::conf_admissions);
        $conf = CollectionHelper::getInstance()
            ->findByFilter("Conf",$filter);
        $conf = $conf[0];

        if( ! $conf->getStatus() ){
            return HttpStatus::Not_Acceptable;
        }

        if( $data['document'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: document is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['document'] === "" ){
            LogHelper::getInstance()->log
            ("Register Student: document is empty!");
            return HttpStatus::Bad_Request;
        }

        if( strlen($data['document']) > DataConstants::Document ){
            LogHelper::getInstance()->log
            ("Register Student: document is too long!");
            return HttpStatus::Bad_Request;
        }

        preg_match_all("/[\d]+/", $data['document'], $coincidencias );
        if( empty($coincidencias) || $coincidencias[0][0] !== $data['document'] ){
            LogHelper::getInstance()->log
            ("Register Student: document invalid format!");
            return HttpStatus::Bad_Request;
        }

        if( $data['password'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: password is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['password'] === "" ){
            LogHelper::getInstance()->log
            ("Register Student: password is empty!");
            return HttpStatus::Bad_Request;
        }

        if( strlen($data['password']) < DataConstants::Pass_min ){
            LogHelper::getInstance()->log
            ("Register Student: Password is too short!");
            return HttpStatus::Bad_Request;
        }

        if( $data['email'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: email is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['email'] === "" ){
            LogHelper::getInstance()->log
            ("Register Student: email is empty!");
            return HttpStatus::Bad_Request;
        }

        if ( ! filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            LogHelper::getInstance()->log
            ("Register Student: email invalid format!");
            return HttpStatus::Bad_Request;
        }

        if( $data['name'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: name is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['name'] === ""){
            LogHelper::getInstance()->log
            ("Register Student: name is empty");
            return HttpStatus::Bad_Request;
        }

        if( $data['surname'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: surname is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['surname'] === ""){
            LogHelper::getInstance()->log
            ("Register Student: surname is empty");
            return HttpStatus::Bad_Request;
        }

        if( $data['username'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: username is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['username'] === ""){
            LogHelper::getInstance()->log
            ("Register Student: username is empty");
            return HttpStatus::Bad_Request;
        }

        if( strlen($data['username']) > DataConstants::Username ){
            LogHelper::getInstance()->log
            ("Register Student: username is too long!");
            return HttpStatus::Bad_Request;
        }

        preg_match_all("/[\w\.\-\_]+/", $data['username'], $coincidencias );
        if( $coincidencias[0][0] !== $data['username'] ){
            LogHelper::getInstance()->log
            ("Register Student: username invalid format!");
            return HttpStatus::Bad_Request;
        }

        if( $data['birthday'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: birthday is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['birthday'] === "" ){
            LogHelper::getInstance()->log
            ("Register Student: birthday is empty!");
            return HttpStatus::Bad_Request;
        }

        try{
            new DateTime($data['birthday']);
        }
        catch (Exception $ex){
            LogHelper::getInstance()->log
            ("Register Student: birthday invalid format!");
            return HttpStatus::Bad_Request;
        }

        $birthday = DateTime::createFromFormat('Y-m-d', $data['birthday']);
        if($birthday->diff(new DateTime())->y < 11) {
            LogHelper::getInstance()->log
            ("Register Student: you are too young !");
            return HttpStatus::Bad_Request;
        }

        if( $data['phones'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: phones array is null");
            return HttpStatus::Bad_Request;
        }

        if( empty($data['phones']) ){
            LogHelper::getInstance()->log
            ("Register Student: phones array is empty");
            return HttpStatus::Bad_Request;
        }

        foreach( $data["phones"] as $phone ){
            if( strlen($phone) < DataConstants::phone_min){
                LogHelper::getInstance()
                    ->log("Register Student: phone is too short");
                return HttpStatus::Bad_Request;
            }

            if( $phone[0] === '+' ){
                $aux = substr($phone, 1);
                preg_match_all("/[\d]+/", $aux, $coincidencias );
                if( $coincidencias[0][0] !== $aux ){
                    return HttpStatus::Bad_Request;
                }
            }
            else{
                preg_match_all("/[\d]+/", $phone, $coincidencias );
                if( $coincidencias[0][0] !== $phone ){
                    return HttpStatus::Bad_Request;
                }
            }
        }

        $filter = Array( "document" => $data['document'] );

        if( CollectionHelper::getInstance()->existsByFilter("User",$filter) ){
            LogHelper::getInstance()->log
            ("Register Student: already exists user with document= "
            .$data['document']);
            return HttpStatus::Conflict;
        }

        $filter = Array( "email" => $data['email'] );

        if( CollectionHelper::getInstance()->existsByFilter("User", $filter) ){
            LogHelper::getInstance()->log
            ("Register Student: already exists user with email= ".
                $data['email']);
            return HttpStatus::Conflict;
        }

        $filter = Array( "username" => $data['username'] );

        if( CollectionHelper::getInstance()->existsByFilter("User",$filter) ){
            LogHelper::getInstance()->log
            ("Register Student: already exists user with username= ".
                $data['username']);
            return HttpStatus::Conflict;
        }

        return HttpStatus::OK;
    }

    public function validateUpdateByStudent( array $data, string $document ) : bool
    {
        $help = new ArrayHelper();
        $arr = Array( "document" => $help->retrieveValueOrNull('document',$data),
                        "name" => $help->retrieveValueOrNull('name',$data),
                        "birthday" => $help->retrieveValueOrNull('birthday',$data),
                        "registeredSince" => $help->retrieveValueOrNull('registeredSince',$data));

        if( $this->cannotChange( $arr ) ){
            return false;
        }

        if( $data === null ){
            LogHelper::getInstance()->log
            ("Update Student: updated info is null");
            return false;
        }

        $filter = Array( "email" => $data['email'] );

        if( $data['email'] !== null ){
            if( UsersController::getInstance()->existsByFilter($filter,$document) ){
                LogHelper::getInstance()->log
                ("Update Student: already exists user with email= ".
                    $data['email']);
                return false;
            }
        }

        return true;
    }

    private function cannotChange( array $arr ) : bool
    {
        foreach ( $arr as $key => $value ){
            if( $value !== null ){
                LogHelper::getInstance()->log
                ("Update Student: students cannot change their ".$key."!" );
                return true;
            }
        }

        return false;
    }

    public function validateUpdateByAdmin( array $data ) : bool
    {
        if( $data['target_document'] === null ){
            LogHelper::getInstance()->log
            ("Update Student: target_document is null" );
            return false;
        }

        if( $data['target_document'] === "" ){
            LogHelper::getInstance()->log
            ("Update Student: target_document is empty");
            return false;
        }

        if( $data['user'] === null ){
            LogHelper::getInstance()->log
            ("Update Student: updated info is null");
            return false;
        }

        $filter = Array( "document" => $data['target_document'] );

        if( ! CollectionHelper::getInstance()
            ->existsByFilter("Student", $filter) ){
            LogHelper::getInstance()->log
            ("Update Student: does NOT exists user with document= "
                .$data['target_document']);
            return false;
        }

        $filter = Array( "document" => $data['user']['document'] );

        if( $data['user']['document'] !== null ){
            if( UsersController::getInstance()
                ->existsByFilter($filter, $data['target_document']) ){
                LogHelper::getInstance()->log
                ("Update Student: already exists user with document= ".
                    $data['user']['document']);
                return false;
            }
        }

        $filter = Array( "email" => $data['user']['email'] );

        if( $data['user']['email'] !== null ){
            if( UsersController::getInstance()
                ->existsByFilter($filter,$data['target_document']) ){
                LogHelper::getInstance()->log
                ("Update Student: already exists user with email= ".
                    $data['user']['email']);
                return false;
            }
        }

        return true;
    }

    public function create(array $data) : int
    {
        $student = new Student( $data );
        return $student->getId();
    }

    public function updateByStudent(array $data, string $document) : string
    {
        $filter = Array( "document" => $document );
        $student = CollectionHelper::getInstance()->findByFilter( "Student",$filter );
        $student = $student[0];
        $student->updateByStudent( $data );

        $GLOBALS['em']->persist($student);
        $GLOBALS['em']->flush();
        return json_encode($data);
    }

    public function updateByAdmin(array $data) : string
    {
        $filter = Array( "document" => $data['target_document'] );
        $student = CollectionHelper::getInstance()->findByFilter( "Student",$filter );
        $student = $student[0];
        $student->updateByAdmin( $data );

        $GLOBALS['em']->persist($student);
        $GLOBALS['em']->flush();
        return json_encode($data);
    }

    public function getSubjectsEnrolled(array $authHeader){
        $arr = array();
        $user = TokenHelper::getInstance()->getUser($authHeader);
        $filter = array( "student" => $user );
        $inscriptions = CollectionHelper::getInstance()
            ->findByFilter("Inscription", $filter);
        if( is_array($inscriptions) ){
            foreach ($inscriptions as $ins){
                array_push($arr, $ins->getSubject()->getId());
            }
        }

        if( empty($arr) ){
            return HttpStatus::Not_Found;
        }
        return $arr;
    }

}