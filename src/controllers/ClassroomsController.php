<?php

class ClassroomsController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new ClassroomsController();
        }
        return self::$instance;
    }

    public function ValidateRegister( array $data ) : int
    {
        if( ! array_key_exists("name", $data) ){
            LogHelper::getInstance()->log
            ("Register Classroom: key name is missing !");
            return HttpStatus::Bad_Request;
        }

        if( $data['name'] === null ){
            LogHelper::getInstance()->log
            ("Register Classroom: name is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['name'] === "" ){
            LogHelper::getInstance()->log
            ("Register Classroom: name is empty!");
            return HttpStatus::Bad_Request;
        }

        if( ! array_key_exists("capacity", $data) ){
            LogHelper::getInstance()->log
            ("Register Classroom: key capacity is missing !");
            return HttpStatus::Bad_Request;
        }

        if( $data['capacity'] === null ){
            LogHelper::getInstance()->log
            ("Register Classroom: capacity is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['capacity'] <= 0 ){
            LogHelper::getInstance()->log
            ("Register Classroom: capacity is equal or lower than 0");
            return HttpStatus::Bad_Request;
        }

        if( ! array_key_exists("floor", $data)){
            LogHelper::getInstance()->log
            ("Register Classroom: key floor is missing !");
            return HttpStatus::Bad_Request;
        }

        if( $data['floor'] === null){
            LogHelper::getInstance()->log
            ("Register Classroom: floor is null!");
            return HttpStatus::Bad_Request;
        }

        $config = include(__DIR__.'/../../minerva_config.php');
        if( $data['floor'] < $config["floors_under"] ||
            $data['floor'] > $config["floors_top"] ){
            LogHelper::getInstance()->log
            ("Register Classroom: floor ".$data['floor']." is out of bounds !");
            return HttpStatus::Bad_Request;
        }

        $filter = Array( "name" => $data['name'] );
        if( CollectionHelper::getInstance()->existsByFilter("Classroom", $filter) ){
            LogHelper::getInstance()->log
            ("Register Classroom: already exists classroom with name= ".
                $data['name']);
            return HttpStatus::Bad_Request;
        }

        if( array_key_exists("preferences", $data) ){
            if( $data["preferences"] === null ){
                LogHelper::getInstance()
                    ->log("Register Classroom: preferences is null !");
                return HttpStatus::Bad_Request;
            }

            if( ! is_array($data["preferences"]) ){
                LogHelper::getInstance()
                    ->log("Register Classroom: preferences is not an array !");
                return HttpStatus::Bad_Request;
            }

            if( empty($data["preferences"]) ){
                LogHelper::getInstance()
                    ->log("Register Classroom: preferences is empty !");
                return HttpStatus::Bad_Request;
            }

            $filter = array( "id" => $data["preferences"]);
            $pref_aux = CollectionHelper::getInstance()
                ->findByFilter("Subject",$filter);
            if( (! is_array($pref_aux)) || count($data["preferences"]) !== count( $pref_aux)){
                LogHelper::getInstance()
                    ->log("Register Classroom: some preference does not exist : ".
                        implode(",",$data["preferences"])." !");
                return HttpStatus::Bad_Request;
            }
        }

        return HttpStatus::OK;
    }

    public function ValidateUpdate( array $data ) : int
    {
        if( ! array_key_exists("id", $data) ){
            LogHelper::getInstance()
                ->log("Update Classroom: id key is missing !");
            return HttpStatus::Bad_Request;
        }

        if( $data["id"] === null ){
            LogHelper::getInstance()
                ->log("Update Classroom: id is null !");
            return HttpStatus::Bad_Request;
        }

        if( array_key_exists("name", $data) ){
            if( $data['name'] === null ){
                LogHelper::getInstance()->log
                ("Register Classroom: name is null!");
                return HttpStatus::Bad_Request;
            }

            if( $data['name'] === "" ){
                LogHelper::getInstance()->log
                ("Register Classroom: name is empty!");
                return HttpStatus::Bad_Request;
            }

            $classroom = CollectionHelper::getInstance()
                ->find("Classroom", $data["id"]);
            $filter = Array( "name" => $data['name'] );
            $classrooms = CollectionHelper::getInstance()
                ->findByFilter("Classroom",$filter);
            if( is_array($classrooms) && count($classroom) == 1 &&
                $classroom->getName() !== $data["name"] ){
                LogHelper::getInstance()
                    ->log("Update Area: name is already taken !");
                return HttpStatus::Conflict;
            }
        }

        if( array_key_exists("capacity", $data)){
            if( $data['capacity'] === null ){
                LogHelper::getInstance()->log
                ("Register Classroom: capacity is null!");
                return HttpStatus::Bad_Request;
            }

            if( $data['capacity'] <= 0 ){
                LogHelper::getInstance()->log
                ("Register Classroom: capacity is equal or lower than 0");
                return HttpStatus::Bad_Request;
            }
        }

        return HttpStatus::OK;
    }

    public function ValidateFree( array $params) : int {

        $arr_helper = new ArrayHelper();
        $params = $arr_helper->castSringToInt($params);

        $keys = array("start","end","day","subject");
        if( $this->aux_format_validation($keys, $params) === HttpStatus::Bad_Request){
            return HttpStatus::Bad_Request;
        }

        if( CollectionHelper::getInstance()
            ->find("Subject",$params["subject"]) === null ){
            LogHelper::getInstance()
                ->log("Free Classrooms: subject ".$params["subject"]
                ." does not exist !");
            return HttpStatus::Bad_Request;
        }

        $bounds = array(
            array( "key" => "start",
                    "value" => $params["start"],
                    "bounds" => DataConstants::period_bounds),
            array( "key" => "end",
                    "value" => $params["end"],
                    "bounds" => DataConstants::period_bounds),
            array( "key" => "day",
                    "value" => $params["day"],
                    "bounds" => DataConstants::weekdays_bounds )
        );
        if( $this->aux_bounds_validation($bounds) === HttpStatus::Bad_Request ){
            return HttpStatus::Bad_Request;
        }
        if( $params["start"] > $params["end"] ){
            LogHelper::getInstance()
                ->log("Free Classrooms: lessons cannot end before they start !");
            return HttpStatus::Bad_Request;
        }

        return HttpStatus::OK;
    }

    private function aux_bounds_validation(array $data) : int {
        foreach ($data as $d){
            if( $d["value"] < $d["bounds"][0] ||
                $d["value"] > $d["bounds"][1]){
                LogHelper::getInstance()
                    ->log("Free Classrooms: ".$d["key"]."= "
                        .$d["value"]." is out of bounds !");
                return HttpStatus::Bad_Request;
            }
        }
        return HttpStatus::OK;
    }

    //Checks nonexistence in array, null and non int
    private function aux_format_validation(array $keys, array $data) : int
    {
        foreach ($keys as $key){
            if( ! array_key_exists($key, $data) ){
                LogHelper::getInstance()
                    ->log("Free Classrooms: key ".$key." is missing !");
                return HttpStatus::Bad_Request;
            }

            if( $data[$key] === null ){
                LogHelper::getInstance()
                    ->log("Free Classrooms: key ".$key." is null !");
                return HttpStatus::Bad_Request;
            }
            //echo $key." ".print_r(gettype($data[$key]));
            if( ! is_int($data[$key])) {
                LogHelper::getInstance()
                    ->log("Free Classrooms: key ".$key." is not a number!");

                return HttpStatus::Bad_Request;
            }
        }
        return HttpStatus::OK;
    }

    public function create(array $data) : int
    {
        $classroom = new Classroom( $data );
        return $classroom->getId();
    }

    public function update(array $data) {
        $classroom = CollectionHelper::getInstance()
            ->find("Classroom", $data["id"] );
        $classroom->update($data);
    }

    public function getClassrooms(){
        $entities = CollectionHelper::getInstance()->findAll("Classroom");
        if( empty($entities) ){
            return HttpStatus::Not_Found;
        }
        $config = include(__DIR__.'/../../minerva_config.php');
        $result = new stdClass();
        $result->bounds = array($config["floors_under"],$config["floors_top"]);
        $result->classrooms = array();
        foreach( $entities as $classroom ){
            array_push($result->classrooms, $classroom->getInfoAll());
        }
        return $result;
    }

    public function getFreeClassrooms(array $data)
    {
        $classrooms = $this->getPreferredClassrooms($data["subject"]);
        if( ! empty($classrooms) ){
            $classrooms = $this->aux_filter_overlap($classrooms,$data);
            if( empty($classrooms) ){
                unset($classrooms);
                $classrooms = CollectionHelper::getInstance()
                    ->findAll("Classroom");
                $classrooms = $this->aux_filter_overlap($classrooms,$data);
                if( empty($classrooms) ){
                    return HttpStatus::Not_Found;
                }
                else{
                    return CollectionHelper::getInstance()->return_ids($classrooms);
                }

            }
            else{
                return CollectionHelper::getInstance()->return_ids($classrooms);
            }
        }
        else{
            unset($classrooms);
            $classrooms = CollectionHelper::getInstance()
                ->findAll("Classroom");
            $classrooms = $this->aux_filter_overlap($classrooms,$data);
            if( empty($classrooms) ){
                return HttpStatus::Not_Found;
            }
            else{
                return CollectionHelper::getInstance()->return_ids($classrooms);
            }
        }
    }

    public function aux_filter_overlap( array $classrooms, array $data) : array {

        $arr = array();
        foreach ( $classrooms as $classroom ){
            $over = false;
            $filter = array( "classroom" => $classroom->getId(),
                "weekday" => $data["day"]);
            //print_r("\nLesson: ".$classroom->getId()."\n");
            $lessons =  CollectionHelper::getInstance()
                ->findByFilter("Lesson", $filter);
            //print_r($lessons);
            /*if( $lessons !== null ){
                print_r(implode(",",CollectionHelper::getInstance()->return_ids($lessons)));
            }
            else{
                print_r("null");
            }*/

            if( is_array($lessons) && ! empty($lessons) ){
                foreach ($lessons as $lesson){
                    if($lesson
                        ->overlaps($data["start"],$data["end"]) === HttpStatus::Conflict){
                        $over = true;
                        break;
                    }
                }
                if( $over === false ){
                    array_push($arr,$classroom);
                }
            }else{
                array_push($arr,$classroom);
            }
        }

        return $arr;
    }

    private function getPreferredClassrooms( int $id_subject) : array {
        $classrooms = array();
        foreach ( CollectionHelper::getInstance()
                      ->findAll("Classroom") as $classroom ){
            if( $classroom->isPreferred($id_subject) ){
                array_push($classrooms, $classroom);
            }
        }
        return $classrooms;
    }

    public function exists( string $name) : bool
    {
        $filter = array( "name" => $name );
        return CollectionHelper::getInstance()
            ->existsByFilter("Classroom", $filter);
    }
}