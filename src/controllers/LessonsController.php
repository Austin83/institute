<?php

class LessonsController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new LessonsController();
        }
        return self::$instance;
    }

    public function validateRegister(array $data) : int
    {
        if( TokenHelper::getInstance()->evaluateAddmissions() === HttpStatus::Not_Acceptable ){
            return HttpStatus::Not_Acceptable;
        }

        $keys = array("id_subject");
        if( $this->aux_format_validation($keys, $data) === HttpStatus::Bad_Request){
            return HttpStatus::Bad_Request;
        }

        if( CollectionHelper::getInstance()
                ->find("Subject", $data["id_subject"]) === null){
            LogHelper::getInstance()
                ->log("Register Curricula: subject with id= "
                    .$data["id_subject"]." does not exist !");
            return HttpStatus::Bad_Request;
        }

        if( ! array_key_exists("week", $data) ){
            LogHelper::getInstance()
                ->log("Register Curricula: week key is missing !");
            return HttpStatus::Bad_Request;
        }
        else{
            if( $data["week"] === null ){
                LogHelper::getInstance()
                    ->log("Register Curricula: key week is null !");
                return HttpStatus::Bad_Request;
            }

            if( ! is_array($data["week"]) ){
                LogHelper::getInstance()
                    ->log("Register Curricula: week is not an array !");
                return HttpStatus::Bad_Request;
            }

            if( empty($data["week"]) ){
                LogHelper::getInstance()
                    ->log("Register Curricula: key week is empty !");
                return HttpStatus::Bad_Request;
            }

            foreach ($data["week"] as $node){
                if( ! is_array($node)){
                    LogHelper::getInstance()
                        ->log("Register Curricula: week is not an object array !");
                    return HttpStatus::Bad_Request;
                }
                else{
                    $keys = array("day","periods","id_teacher","id_classroom");
                    if( $this->aux_format_validation($keys, $node) === HttpStatus::Bad_Request){
                        return HttpStatus::Bad_Request;
                    }

                    if( count($node["id_teacher"]) !== DataConstants::shifts ){
                        LogHelper::getInstance()
                            ->log("Register Curricula: not enough teachers !");
                        return HttpStatus::Bad_Request;
                    }

                    if( count($node["id_classroom"]) !== DataConstants::shifts ){
                        LogHelper::getInstance()
                            ->log("Register Curricula: not enough classrooms !");
                        return HttpStatus::Bad_Request;
                    }

                    if( $node["day"] < DataConstants::weekdays_bounds[0] ||
                        $node["day"] > DataConstants::weekdays_bounds[1]){
                        LogHelper::getInstance()
                            ->log("Register Curricula: day= ".$node["day"]
                                ." is out of bounds");
                        if( $node["day"] === WeekDays::Sunday ){
                            LogHelper::getInstance()
                                ->log("Register Curricula: God chilled on Sunday, who are you to choose otherwise ?");
                        }
                        return HttpStatus::Bad_Request;
                    }

                    if( is_array($node["periods"]) ){
                        if( $node["periods"][0] < DataConstants::period_bounds[0] ||
                            $node["periods"][0] > DataConstants::period_bounds[1]){
                            LogHelper::getInstance()
                                ->log("Register Curricula: period_start out of bounds !");
                            return HttpStatus::Bad_Request;
                        }

                        if( $node["periods"][1] < DataConstants::period_bounds[0] ||
                            $node["periods"][1] > DataConstants::period_bounds[1]){
                            LogHelper::getInstance()
                                ->log("Register Curricula: period_end out of bounds !");
                            return HttpStatus::Bad_Request;
                        }

                        if( $node["periods"][0] > $node["periods"][1] ){
                            LogHelper::getInstance()
                                ->log("Register Curricula: lessons cannot end before they start!");
                            return HttpStatus::Bad_Request;
                        }
                    }
                    else{
                        if( $node["periods"] < DataConstants::period_bounds[0] ||
                            $node["periods"] > DataConstants::period_bounds[1]){
                            LogHelper::getInstance()
                                ->log("Register Curricula: period_start out of bounds !");
                            return HttpStatus::Bad_Request;
                        }
                    }

                    $target = "target";
                    $id = "id";

                    $batch = array(
                                array($target => "Teacher", $id => $node["id_teacher"]),
                                array($target => "Classroom", $id => $node["id_classroom"]));

                    if( $this->aux_entities_validation($batch, $data["id_subject"])
                        === HttpStatus::Bad_Request ){
                        return HttpStatus::Bad_Request;
                    }
                }
            }
        }

        return HttpStatus::OK;
    }

    public function getLessons(array $authHeader){
        $user = TokenHelper::getInstance()->getUser($authHeader);
        $now = new DateTime();
        $now->setTime(0, 0, 0);
        $begin = null;
        $end = null;

        if ($now->format('N') == WeekDays::Monday) {
            $begin = $now;
        } else {
            $begin = $now->modify('last monday');
        }

        $dates = array();
        for( $i=1; $i < 6; $i++ ){
            array_push($dates, $begin->modify('+1 day')->format('Y-m-d'));
        }
        foreach ($dates as $date){
            $date = DateTime::createFromFormat('Y-m-d',$date);
        }

        $filter = array("date" => $dates);
        $lessons = CollectionHelper::getInstance()
            ->findByFilter("Lesson",$filter);
        $subjects = CollectionHelper::getInstance()->findAll("Subject");
        $result = new stdClass();
        foreach ( $subjects as $subject ){
            $result->{$subject->getId()} = array();
            if(  is_array($lessons) ){
                foreach ($lessons as $lesson){
                    if( $lesson->getSubject()->getId() === $subject->getId() ){
                        array_push($result->{$subject->getId()},$lesson->getInfoAll($user));
                    }
                }
                if( empty($result->{$subject->getId()})){
                    unset($result->{$subject->getId()});
                }
            }
        }

        return $result;
    }

    //Checks nonexistence in array, null and not int array or int
    private function aux_format_validation(array $keys, array $data) : int
    {
        foreach ($keys as $key){
            if( ! array_key_exists($key, $data) ){
                LogHelper::getInstance()
                    ->log("Register Curricula: key ".$key." is missing !");
                return HttpStatus::Bad_Request;
            }

            if( $data[$key] === null ){
                LogHelper::getInstance()
                    ->log("Register Curricula: key ".$key." is null !");
                return HttpStatus::Bad_Request;
            }

            if( is_array($data[$key])  ){
                foreach ($data[$key] as $number)
                    if( ! is_int($number) ){
                        LogHelper::getInstance()
                            ->log("Register Curricula: key "
                                .$key." is not a number array!");

                        return HttpStatus::Bad_Request;
                    }
            }
            else if( ! is_int($data[$key])) {
                LogHelper::getInstance()
                    ->log("Register Curricula: key "
                        .$key." is not a number!");

                return HttpStatus::Bad_Request;
            }
        }
        return HttpStatus::OK;
    }

    //Checks nonexistence for Teacher and Classroom
    private function aux_entities_validation( array $batch, int $id_subject ){
        foreach ( $batch as $node ){
            foreach ( Shifts::All as $shift ){
                $entity = CollectionHelper::getInstance()
                    ->find($node["target"], $node["id"][$shift]);
                if( $entity === null){
                    return HttpStatus::Bad_Request;
                }
                else{
                    if( $node["target"] === "Teacher" ){
                        if( ! $entity->getApproved() ){
                            LogHelper::getInstance()
                                ->log("Register Curricula: teacher= "
                                    .$entity->getId()." is not approved !");
                            return HttpStatus::Bad_Request;
                        }
                        if( $entity->canTeach($id_subject)
                            === HttpStatus::Not_Found){
                            LogHelper::getInstance()
                                ->log("Register Curricula: teacher= ".
                                    $entity->getId()." cannot teach subject= ".
                                    $id_subject);
                            return HttpStatus::Bad_Request;
                        }
                    }
                }
            }
        }
        return HttpStatus::OK;
    }

    public function createBatch(array $data)
    {
        $connection = $GLOBALS['em']->getConnection();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $connection
            ->query('DELETE FROM lessons WHERE subject_id='.$data["id_subject"]);
        $connection
            ->query('DELETE FROM impartations WHERE subject='.$data["id_subject"]);
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
        $sql = "ALTER TABLE lessons AUTO_INCREMENT = 1";
        $command = $GLOBALS['em']->getConnection()->prepare($sql);
        $command->execute();
        $sql = "ALTER TABLE impartations AUTO_INCREMENT = 1";
        $command = $GLOBALS['em']->getConnection()->prepare($sql);
        $command->execute();

        $year = date("Y");
        $date_index = date($year.DataConstants::school_year[0]);
        $date_index = DateTime::createFromFormat('Y-m-d H:i:s', $date_index);

        while( $date_index < new DateTime($year.DataConstants::school_year[1]))
        {
            $day = $this->getWeekday( DateTime::createFromFormat("Y-m-d",
                $date_index->format("Y-m-d")));
            foreach ($data["week"] as $node){
                if($node["day"] === $day){
                    foreach(Shifts::All as $shift){
                        $data_aux = array(
                            "shift" => $shift,
                            "day" => $day,
                            "stamp" => $date_index->format("Y-m-d"));
                        $lesson = new Lesson($node, $data_aux, $data["id_subject"]);
                        new Impartation($node["id_teacher"][$shift],
                                        $lesson, $data["id_subject"]);
                    }
                }
            }

            $date_index->add(new DateInterval('P1D'));
        }
        $GLOBALS['em']->flush();
    }

    private function getWeekday( DateTime $date) : int {
        return date('N', $date->getTimestamp() );
    }

}