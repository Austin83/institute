<?php

class AreasController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new AreasController();
        }
        return self::$instance;
    }

    public function validateRegister( array $data) : int
    {
        if( $data['name'] === null || $data['name'] === "" ){
            LogHelper::getInstance()->log
            ("Bad_Request => Register Area => name is null or empty!");
            return HttpStatus::Bad_Request;
        }
        $filter = Array( "name" => $data['name'] );
        if( CollectionHelper::getInstance()->existsByFilter("Area", $filter ) ){
            LogHelper::getInstance()->log
            ("Conflict => Register Area => already exists Area with name= "
                .$data['name']);
            return HttpStatus::Conflict;
        }

        if( array_key_exists("id_teacher", $data)){

            if( $data["id_teacher"] !== null &&
                CollectionHelper::getInstance()
                    ->find("Teacher", $data["id_teacher"]) === null){
                LogHelper::getInstance()
                    ->log("Update Area: teacher with id= ".$data["id_teacher"]
                        ." does not exists !");
                return HttpStatus::Not_Found;
            }
        }

        return HttpStatus::OK;
    }

    public function validateUpdate( array $data) : int
    {
        if( ! array_key_exists("id", $data)){
            LogHelper::getInstance()
                ->log("Update Area: Key missing => id !");
            return HttpStatus::Bad_Request;
        }

        if( $data["id"] === null ){
            LogHelper::getInstance()
                ->log("Update Area: id is null !");
            return HttpStatus::Bad_Request;
        }

        if( empty($data["id"])){
            LogHelper::getInstance()
                ->log("Update Area: id is empty !");
            return HttpStatus::Bad_Request;
        }

        $area = CollectionHelper::getInstance()->find("Area", $data["id"]);

        if( array_key_exists("name", $data)){

            if( $data["name"] === null ){
                LogHelper::getInstance()
                    ->log("Update Area: name is null !");
                return HttpStatus::Bad_Request;
            }

            if( empty($data["name"]) ){
                LogHelper::getInstance()
                    ->log("Update Area: name is empty !");
                return HttpStatus::Bad_Request;
            }

            $filter = array( "name" => $data["name"] );
            $areas = CollectionHelper::getInstance()->findByFilter("Area",$filter);
            if( is_array($areas) && count($areas)  == 1 && $area->getName() !== $data["name"] ){
                LogHelper::getInstance()
                    ->log("Update Area: name is already taken !");
                return HttpStatus::Conflict;
            }
        }

        if( array_key_exists("id_teacher", $data)){
            if( $data["id_teacher"] !== null ){
                if(CollectionHelper::getInstance()
                        ->find("Teacher", $data["id_teacher"]) === null){
                    LogHelper::getInstance()
                        ->log("Update Area: teacher with id= ".$data["id_teacher"]
                            ." does not exists !");
                    return HttpStatus::Not_Found;
                }
            }
        }

        return HttpStatus::OK;
    }

    public function create(array $data) : int
    {
        $area = new Area( $data );
        return $area->getId();
    }

    public function update(array $data){
        $area = CollectionHelper::getInstance()->find("Area", $data["id"]);
        $area->update($data);
    }

    public function exists( string $name ) : bool {
        $filter = array( "name" => $name );
        return CollectionHelper::getInstance()
            ->existsByFilter("Area", $filter);
    }

    public function getAreas(InfoHelper $filter_area){
        $entities = CollectionHelper::getInstance()->findAll("Area");
        if( empty($entities) ){
            return HttpStatus::Not_Found;
        }
        $result = array();
        foreach( $entities as $area ){
            array_push($result, $area->getInfo($filter_area));
        }
        return $result;
    }

}