<?php

class AdminsController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new AdminsController();
        }
        return self::$instance;
    }

    public function validateRegister( array $data ) : bool
    {
        if( $data === null ){
            LogHelper::getInstance()->log
            ("Register Admin: body is null!");
            return false;
        }

        if( empty($data) ){
            LogHelper::getInstance()->log
            ("Register Admin: body is empty!");
            return false;
        }

        if( $data['document'] === null ){
            LogHelper::getInstance()->log
            ("Register Admin: document is null!");
            return false;
        }

        if( $data['document'] === "" ){
            LogHelper::getInstance()->log
            ("Register Admin: document is empty!");
            return false;
        }

        if( $data['password'] === null ){
            LogHelper::getInstance()->log
            ("Register Admin: password is null!");
            return false;
        }

        if( $data['password'] === "" ){
            LogHelper::getInstance()->log
            ("Register Admin: password is empty!");
            return false;
        }

        if( $data['email'] === null ){
            LogHelper::getInstance()->log
            ("Register Admin: email is null!");
            return false;
        }

        if( $data['email'] === "" ){
            LogHelper::getInstance()->log
            ("Register Admin: email is empty!");
            return false;
        }

        if( $data['name'] === null ){
            LogHelper::getInstance()->log
            ("Register Admin: name is null!");
            return false;
        }

        if( $data['name'] === ""){
            LogHelper::getInstance()->log
            ("Register Admin: name is empty");
            return false;
        }

        if( $data['birthday'] === null ){
            LogHelper::getInstance()->log
            ("Register Admin: birthday is null!");
            return false;
        }

        if( $data['birthday'] === "" ){
            LogHelper::getInstance()->log
            ("Register Admin: birthday is empty!");
            return false;
        }

        if( $data['phones'] === null ){
            LogHelper::getInstance()->log
            ("Register Admin: phones array is null");
            return false;
        }

        if( empty($data['phones']) ){
            LogHelper::getInstance()->log
            ("Register Admin: phones array is empty");
            return false;
        }

        $filter = Array( "document" => $data['document'] );

        if( CollectionHelper::getInstance()->existsByFilter("User",$filter) ){
            LogHelper::getInstance()->log
            ("Register Admin: already exists user with document= "
                .$data['document']);
            return false;
        }

        $filter = Array( "email" => $data['email'] );

        if( CollectionHelper::getInstance()->existsByFilter("User", $filter) ){
            LogHelper::getInstance()->log
            ("Register Admin: already exists user with email= ".
                $data['email']);
            return false;
        }

        $filter = Array( "username" => $data['username'] );

        if( CollectionHelper::getInstance()->existsByFilter("User",$filter) ){
            LogHelper::getInstance()->log
            ("Register Admin: already exists user with username= ".
                $data['username']);
            return false;
        }

        return true;
    }

    public function create(array $data) : Admin
    {
        $admin = new Admin( $data );
        $GLOBALS['em']->persist($admin);
        $GLOBALS['em']->flush();
        return $admin;
    }

    public function validateAdmissionSwitch($state) : int {

        if( ! isset($state) ){
            LogHelper::getInstance()
                ->log("Admissions Switch: state was not given !");
            return HttpStatus::Bad_Request;
        }

        if( ! is_bool($state) ){
            LogHelper::getInstance()
                ->log("Admissions Switch: state was not bool !");
            return HttpStatus::Bad_Request;
        }

        $filter = array("name" => DataConstants::conf_admissions);
        $conf = CollectionHelper::getInstance()
            ->findByFilter("Conf",$filter);
        $conf = $conf[0];

        if( $conf->getStatus() === $state){
            LogHelper::getInstance()
                ->log("Admissions Switch: value is already ".$state." !");
            return HttpStatus::Conflict;
        }

        return HttpStatus::OK;
    }

    public function admissions_switch(bool $state)
    {
        $filter = array("name" => DataConstants::conf_admissions);
        $conf = CollectionHelper::getInstance()
            ->findByFilter("Conf",$filter);
        $conf = $conf[0];
        $conf->setStatus($state);
        $GLOBALS["em"]->persist($conf);
        $GLOBALS["em"]->flush();
    }

    public function getAdmissions() : bool
    {
        $filter = array("name" => DataConstants::conf_admissions);
        $conf = CollectionHelper::getInstance()
            ->findByFilter("Conf",$filter);
        $conf = $conf[0];
        return $conf->getStatus();
    }
}