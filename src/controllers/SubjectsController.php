<?php

class SubjectsController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new SubjectsController();
        }
        return self::$instance;
    }

    public function validateRegister( array $data ) : int
    {
        if ( ! self::validateNullOrEmpty($data) ){
            return HttpStatus::Bad_Request;
        }

        $filter = Array( "name" => $data['name'] );

        if ( CollectionHelper::getInstance()->existsByFilter("Subject", $filter) ) {
            LogHelper::getInstance()->log
            ("Register Subject: already exist a subject with name='".$data['name']."'");
            return HttpStatus::Conflict;
        }

        if( $data['area_id'] === null ){
            LogHelper::getInstance()->log
            ("Register Subject: 'area_id' is null");
            return HttpStatus::Bad_Request;
        }

        if( $data['area_id'] === "" ){
            LogHelper::getInstance()->log
            ("Register Subject: 'area_id' is empty");
            return HttpStatus::Bad_Request;
        }

        $area = CollectionHelper::getInstance()
            ->find( "Area", $data['area_id'] );
        if( $area === null ){
            LogHelper::getInstance()->log
            ("Register Subject: area not found with id= ".$data['area_id'] );
            return HttpStatus::Bad_Request;
        }

        if( ! self::validateTopicsUniqueName( $data ) ) {
            LogHelper::getInstance()->log
            ("Register Subject: non unique topic names");
            return HttpStatus::Bad_Request;
        }

        if( ! self::validateTopicsChildrenUnique( $data ) ){
            LogHelper::getInstance()->log(
                "Register Subject: non unique topic names on subject ".$data["name"]. " !"
            );
            return HttpStatus::Bad_Request;
        }

        if( array_key_exists("prior_ids", $data) ){
            if( ! self::validatePriorSubjects( $data['prior_ids'] ) ) {
                return HttpStatus::Bad_Request;
            }
        }

        return HttpStatus::OK;
    }

    public function validateUpdate( array $data ) : int
    {
        if( ! array_key_exists("id", $data) ){
            LogHelper::getInstance()
            ->log("Update Subject: key is missing !");
            return HttpStatus::Bad_Request;
        }

        if( $data["id"] === null ){
            LogHelper::getInstance()
                ->log("Update Subject: id is null !");
            return HttpStatus::Bad_Request;
        }

        if( array_key_exists("name", $data) ){
            if( empty($data["name"])){
                LogHelper::getInstance()
                    ->log("Update Subject: name is empty !");
                return HttpStatus::Bad_Request;
            }

            if( $data["name"] === null){
                LogHelper::getInstance()
                    ->log("Update Subject: name is null !");
                return HttpStatus::Bad_Request;
            }

            $subject = CollectionHelper::getInstance()
                ->find("Subject", $data["id"]);
            $filter = Array( "name" => $data['name'] );
            $subjects = CollectionHelper::getInstance()
                ->findByFilter("Subject",$filter);
            if( is_array($subjects) && count($subjects) == 1 && $subject->getName() !== $data["name"] ){
                LogHelper::getInstance()
                    ->log("Update Subject: name is already taken !");
                return HttpStatus::Conflict;
            }
        }

        if( array_key_exists("topics", $data) ){
            if( $data["topics"] === null ){
                LogHelper::getInstance()
                    ->log("Update SUbject: topics key is null !");
            }

            if( ! self::validateTopicsUniqueName( $data ) ) {
                LogHelper::getInstance()->log
                ("Update Subject: non unique topic names");
                return HttpStatus::Bad_Request;
            }

            if( ! self::validateTopicsChildrenUnique( $data ) ){
                return HttpStatus::Bad_Request;
            }
        }

        if( array_key_exists("priors", $data)){
            if( $data["priors"] === null ){
                LogHelper::getInstance()
                    ->log("Update Subject: priors key is null !");
                return HttpStatus::Bad_Request;
            }

            if( ! is_array($data["priors"]) ){
                LogHelper::getInstance()
                    ->log("Update Subject: priors is not an array !");
                return HttpStatus::Bad_Request;
            }

            foreach ($data["priors"] as $prior){
                if( ! is_int($prior) ){
                    LogHelper::getInstance()
                        ->log("Update Subject: priors array => ".$prior
                            ." is not of type int !");
                    return HttpStatus::Bad_Request;
                }
                else{
                    if( CollectionHelper::getInstance()
                        ->find("Subject", $prior) === null){
                        LogHelper::getInstance()
                            ->log("Update Subject: subject ".$prior
                                ." does not exists !");
                        return HttpStatus::Bad_Request;
                    }
                }
            }
        }

        return HttpStatus::OK;
    }

    private function validateNullOrEmpty( array $data ) : bool
    {

        if ( $data['name'] === null ) {
            LogHelper::getInstance()->log
            ("Register Subject: name came null");
            return false;
        }

        if ( $data['name'] === '' ) {
            LogHelper::getInstance()->log
            ("Register Subject: name came empty");
            return false;
        }

        if( ! array_key_exists("topics",$data)){
            LogHelper::getInstance()
                ->log("Register Subject: topics key is missing !");
            return false;
        }

        if ($data['topics'] === null ) {
            LogHelper::getInstance()->log
            ("Register Subject: topics came null");
            return false;
        }

        if ( empty($data['topics']) ) {
            LogHelper::getInstance()->log
            ("Register Subject: topics came empty");
            return false;
        }

        foreach ($data['topics'] as $item) {

            if ( $item['name'] === null ) {
                LogHelper::getInstance()->log
                ("Register Subject: A topic's name in topics came null");
                return false;
            }

            if ( $item['name'] === '' ) {
                LogHelper::getInstance()->log
                ("Register Subject: A topic's name in topics came empty");
                return false;
            }
        }

        return true;
    }

    private function validateTopicsChildrenUnique( array $data ) : bool
    {
        foreach ( $data['topics'] as $topic) {
            if( $topic['children'] === null ){
                continue;
            }

            if( self::topic_has_duplicate_children( $topic ) ){
                    return false;
            }
        }
        return true;
    }

    private function topic_has_duplicate_children(array $topic) {
        $aux = array();
        foreach( $topic['children'] as $child ){
            array_push( $aux, $child['name'] );
        }
        $aux = array_unique($aux);
        return count( $topic['children'] ) !== count( $aux );
    }

    private function validateTopicsUniqueName( array $data ) : bool {

        $topic_names = array();

        foreach ( $data['topics'] as $topic){
            array_push( $topic_names, $topic['name'] );
        }

        $aux = array_unique($topic_names);
        if( count( $data['topics'] ) !== count( $aux ) ){
            LogHelper::getInstance()->log
            ("Register Subject: first level deep topics have duplicate names");
            return false;
        }

        return true;
    }

    private function validatePriorSubjects( $priors )
    {
        if( $priors === null || empty( $priors )){
            return false;
        }
            foreach ( $priors as $id ){
                if( $id !== null ){
                    if( CollectionHelper::getInstance()->find("Subject",$id) === null ){
                        LogHelper::getInstance()->log
                        ("Register Subject: does not exists a subject with id=".$id );
                        return false;
                    }
                }
                else{
                    LogHelper::getInstance()->log
                    ("Register Subject: one or more prior subject ids came null" );
                    return false;
                }
            }

        return true;
    }

    public function create( array $data) : int
    {
        $subject = new Subject($data);
        return $subject->getId();
    }

    public function update( array $data)
    {
        $subject = CollectionHelper::getInstance()
            ->find("Subject",$data["id"]);
        $subject->update($data);
    }

    public function getSubjects(InfoHelper $filter_subjects,
                                InfoHelper $filter_topics){
        $entities = CollectionHelper::getInstance()->findAll("Subject");
        if( empty($entities) ){
            return HttpStatus::Not_Found;
        }
        $result = array();
        foreach( $entities as $subject ){
            array_push($result, $subject->getInfo($filter_subjects,
                                                    $filter_topics));
        }
        return $result;
    }

    public function exists( string $name) : bool
    {
        $filter = array( "name" => $name );
        return CollectionHelper::getInstance()
            ->existsByFilter("Subject", $filter);
    }
}