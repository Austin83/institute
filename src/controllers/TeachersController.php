<?php

class TeachersController
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new TeachersController();
        }
        return self::$instance;
    }

    public function validateRegister( array $data ) : int
    {
        if( TokenHelper::getInstance()->evaluateAddmissions() === HttpStatus::Not_Acceptable ){
            return HttpStatus::Not_Acceptable;
        }

        if( $data['document'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: document is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['document'] === "" ){
            LogHelper::getInstance()->log
            ("Register Teacher: document is empty!");
            return HttpStatus::Bad_Request;
        }

        if( strlen($data['document']) > DataConstants::Document ){
            LogHelper::getInstance()->log
            ("Register Teacher: document is too long!");
            return HttpStatus::Bad_Request;
        }

        preg_match_all("/[\d]+/", $data['document'], $coincidencias );
        if( empty($coincidencias) || $coincidencias[0][0] !== $data['document'] ){
            LogHelper::getInstance()->log
            ("Register Teacher: document invalid format!");
            return HttpStatus::Bad_Request;
        }

        if( $data['username'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: username is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['username'] === "" ){
            LogHelper::getInstance()->log
            ("Register Teacher: username is empty!");
            return HttpStatus::Bad_Request;
        }

        if( $data['password'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: password is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['password'] === "" ){
            LogHelper::getInstance()->log
            ("Register Teacher: password is empty!");
            return HttpStatus::Bad_Request;
        }

        if( strlen($data['password']) < DataConstants::Pass_min ){
            LogHelper::getInstance()->log
            ("Register Teacher: Password is too short!");
            return HttpStatus::Bad_Request;
        }

        if( $data['email'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: email is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['email'] === "" ){
            LogHelper::getInstance()->log
            ("Register Teacher: email is empty!");
            return HttpStatus::Bad_Request;
        }

        if ( ! filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            LogHelper::getInstance()->log
            ("Register Teacher: email invalid format!");
            return HttpStatus::Bad_Request;
        }

        if( $data['name'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: name is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['name'] === "" ){
            LogHelper::getInstance()->log
            ("Register Teacher: name is empty!");
            return HttpStatus::Bad_Request;
        }

        if( $data['surname'] === null ){
            LogHelper::getInstance()->log
            ("Register Student: surname is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['surname'] === ""){
            LogHelper::getInstance()->log
            ("Register Student: surname is empty");
            return HttpStatus::Bad_Request;
        }

        if( $data['birthday'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: birthday is null!");
            return HttpStatus::Bad_Request;
        }

        if( $data['birthday'] === "" ){
            LogHelper::getInstance()->log
            ("Register Teacher: birthday is empty!");
            return HttpStatus::Bad_Request;
        }

        try{
            new DateTime($data['birthday']);
        }
        catch (Exception $ex){
            LogHelper::getInstance()->log
            ("Register Teacher: birthday invalid format!");
            return HttpStatus::Bad_Request;
        }

        if( $data['phones'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: phones array is null");
            return HttpStatus::Bad_Request;
        }

        if( empty($data['phones']) ){
            LogHelper::getInstance()->log
            ("Register Teacher: phones array is empty");
            return HttpStatus::Bad_Request;
        }

        if( $data['subjects'] === null ){
            LogHelper::getInstance()->log
            ("Register Teacher: subjects array is null");
            return HttpStatus::Bad_Request;
        }

        if( ! is_array($data['subjects']) ){
            LogHelper::getInstance()->log
            ("Register Teacher: subjects is not an array !");
            return HttpStatus::Bad_Request;
        }

        if( empty($data['subjects']) ){
            LogHelper::getInstance()->log
            ("Register Teacher: subjects array is empty");
            return HttpStatus::Bad_Request;
        }

        foreach ($data["subjects"] as $subject){
            if( ! is_int($subject) ){
                LogHelper::getInstance()
                    ->log("Register Teacher: subjects => "
                        .$subject." is not an int !");
                return HttpStatus::Bad_Request;
            }
            if( CollectionHelper::getInstance()
                ->find("Subject", $subject) === null){
                LogHelper::getInstance()
                    ->log("Register Teacher: subject ".$subject." does not exist !");
                return HttpStatus::Bad_Request;
            }
        }

        $filter = Array( "document" => $data['document'] );

        if( CollectionHelper::getInstance()->existsByFilter("User",$filter) ){
            LogHelper::getInstance()->log
            ("Register Teacher: user already exists with document= ".
                $data['document']." !");
            return HttpStatus::Conflict;
        }

        $filter = Array( "email" => $data['email'] );

        if( CollectionHelper::getInstance()->existsByFilter("User", $filter) ){
            LogHelper::getInstance()->log
            ("Register Teacher: already exists user with email= ".
                $data['email']);
            return HttpStatus::Conflict;
        }

        $filter = Array( "username" => $data['username'] );

        if( CollectionHelper::getInstance()->existsByFilter("User",$filter) ){
            LogHelper::getInstance()->log
            ("Register Teacher: already exists user with username= ".
                $data['username']);
            return HttpStatus::Conflict;
        }

        return HttpStatus::OK;
    }

    public function validateUpdateByTeacher( array $data, string $document ) : bool
    {
        $help = new ArrayHelper();

        $arr = Array( "document" => $help->retrieveValueOrNull('document',$data),
            "name" => $help->retrieveValueOrNull('name',$data),
            "birthday" => $help->retrieveValueOrNull('birthday',$data),
            "registeredSince" => $help->retrieveValueOrNull('registeredSince',$data),
            "grade" => $help->retrieveValueOrNull('grade',
                $help->retrieveValueOrNull('teacher',$data)));

        if( $this->cannotChange( $arr ) ){
            return false;
        }

        if( $data['email'] !== null ){
            $filter = Array( "email" => $data['email'] );
            if( UsersController::getInstance()
                ->existsByFilter($filter,$document) ){
                LogHelper::getInstance()->log
                ("Update Student: already exists user with email= ".
                    $data['email']);
                return false;
            }
        }

        return true;
    }

    private function cannotChange( array $arr ) : bool
    {
        foreach ( $arr as $key => $value ){
            if( $value !== null ){
                LogHelper::getInstance()->log
                ("Update Teacher: teachers cannot change their ".$key."!" );
                return true;
            }
        }

        return false;
    }

    public function validateUpdateByAdmin( array $data ) : bool
    {
        if( $data['target_document'] === null ){
            LogHelper::getInstance()->log
            ("Update Teacher: target_document is null!");
            return false;
        }

        if( $data['target_document'] === "" ) {
            LogHelper::getInstance()->log
            ("Update Teacher: target_document is empty!");
            return false;
        }

        if( $data['user'] === null ){
            LogHelper::getInstance()->log
            ("Update Teacher: user update object is null!");
            return false;
        }

        if( empty( $data['user'] ) ){
            LogHelper::getInstance()->log
            ("Update Teacher: user update object is empty!");
            return false;
        }

        $filter = Array( "document" => $data['target_document'] );

        if( ! CollectionHelper::getInstance()
            ->existsByFilter("Teacher", $filter) ){
            LogHelper::getInstance()->log
            ("Update Teacher: does NOT exists teacher with document= "
                .$data['target_document']);
            return false;
        }

        if( $data['user']['document'] !== null ){
            if( $data['user']['document'] === "" ){
                LogHelper::getInstance()->log
                ("Update Teacher: user's new document came empty!");
                return false;
            }
            else{
                $filter = Array( "document" => $data['user']['document'] );
                if( UsersController::getInstance()
                    ->existsByFilter($filter, $data['target_document']) ){
                    LogHelper::getInstance()->log
                    ("Update Teacher: teacher already exists with document= ".
                        $data['user']['document']);
                    return false;
                }
            }
        }

        if( $data['user']['email'] !== null ){
            $filter = Array( "email" => $data['user']['email'] );
            if( UsersController::getInstance()
                ->existsByFilter($filter,$data['target_document']) ){
                LogHelper::getInstance()->log
                ("Update Student: already exists user with email= ".
                    $data['user']['email']);
                return false;
            }
        }

        return true;
    }

    public function create(array $data) : int
    {
        $teacher = new Teacher($data);
        return $teacher->getId();
    }

    public function updateByTeacher(array $data, string $document) : string
    {
        $filter = Array( "document" => $document );
        $teacher = CollectionHelper::getInstance()->findByFilter("Teacher",$filter);
        $teacher = $teacher[0];
        $teacher->updateByTeacher( $data );
        $GLOBALS['em']->persist($teacher);
        $GLOBALS['em']->flush();
        return json_encode($data);
    }

    public function updateByAdmin(array $data) : string
    {
        $filter = Array( "document" => $data['target_document'] );
        $teacher = CollectionHelper::getInstance()->findByFilter("Teacher",$filter);
        $teacher = $teacher[0];
        $teacher->updateByAdmin( $data );
        $GLOBALS['em']->persist($teacher);
        $GLOBALS['em']->flush();
        return json_encode($data);
    }

    public function getTeachers( InfoHelper $filter_teacher){
        $arr = array("approved" => true);
        $entities = CollectionHelper::getInstance()
            ->findByFilter("Teacher", $arr);
        if( empty($entities) ){
            return HttpStatus::Not_Found;
        }
        $result = array();
        foreach( $entities as $teacher ){
            array_push($result, $teacher->getInfo($filter_teacher));
        }
        return $result;
    }
}