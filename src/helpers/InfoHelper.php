<?php

class InfoHelper
{
    private $filter;
    private $mode;

    public function __construct( array $filter, int $mode ){
        $this->filter = $filter;
        $this->mode = $mode;
    }

    /**
     * @return array
     */
    public function getFilter(): array
    {
        return $this->filter;
    }

    /**
     * @return int
     */
    public function getMode(): int
    {
        return $this->mode;
    }


}