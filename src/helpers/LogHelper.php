<?php

class LogHelper
{
    private static $instance;
    private static $logPath = getenv('HOME')."/minerva-backend/public/log.txt";

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new LogHelper();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $log = fopen (self::$logPath, 'a+') or die("Unable to create file in directory: ".$logPath."!");
        fwrite( $log, "\n" );
        fclose( $log );
    }

    public function log( string $text ){
        $log = fopen (self::$logPath, "a") or die("Unable to open in directory: ".$logPath."!");
        $timeZone = 'America/Montevideo';
        $date = new DateTime("now", new DateTimeZone($timeZone) );
        $date = $date->format("G:i:s d/m/Y ");
        fwrite( $log, $date .$text."\n" );
        fclose( $log );
    }
}
