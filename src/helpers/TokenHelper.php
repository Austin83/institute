<?php

use \Firebase\JWT\JWT;
use \Slim\Http\Request;
use \Slim\Http\Response;

class TokenHelper
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new TokenHelper();
        }
        return self::$instance;
    }

    public function create( bool $credentialsAreValid, array $user ){
        if ($credentialsAreValid) {
            $config = include(__DIR__.'/../../minerva_config.php');

            $tokenId = base64_encode(random_bytes (32));
            $issuedAt = time();
            $notBefore = $issuedAt;//Adding 10 seconds
            $expire = $notBefore + $GLOBALS['tokenDuration'];// Adding seconds
            $serverName = $config['serverName'];// Retrieve the server name from config file

            $data = [
                'iat' => $issuedAt,// Issued at: time when the token was generated
                'jti' => $tokenId,// Json Token Id: an unique identifier for the token
                'iss' => $serverName,// Issuer
                'nbf' => $notBefore,// Not before
                'exp' => $expire,// Expire
                'data' => [ // Data related to the signer user
                    'id' => $user['id'],//user Id from the users table
                    'username' => $user['username'],//User Name
                    'type' => $user['type'],//User Type
                ]
            ];

            $secretKey = base64_decode($config['jwtKey']);
            $jwt = JWT::encode(
                $data,      //Data to be encoded in the JWT
                $secretKey, // The signing key
                'HS512'     // Algorithm used to sign the token
            );
            return $jwt;
        }
        return null;
    }

    public function check( array $authHeader ) : array
    {
        if ($authHeader) {

            list($jwt) = sscanf( $authHeader[0], 'Bearer %s');
            if ($jwt) {
                try {
                    $config = include(__DIR__.'/../../minerva_config.php');

                    //decode the jwt using the key from config
                    $secretKey = base64_decode($config['jwtKey']);
                    $token = JWT::decode($jwt, $secretKey, array('HS512'));
                    return array('success' => 'true', 'token' => $token, 'status' => 200 );

                } catch (Exception $e) {
                    $message = "The token was not able to be decoded. This is likely because the signature was not able to be verified (tampered token)";
                    LogHelper::getInstance()->log( $message );
                    return array('success' => 'false', 'message' => $message, 'status' => 401 );
                }
            } else {
                $message = "No token was able to be extracted from the authorization header.";
                LogHelper::getInstance()->log( $message );
                return array('success' => 'false', 'message' => $message, 'status' => 400 );
            }
        } else {
            $message = "Token not found in request";
            LogHelper::getInstance()->log( $message );
            return array('success' => 'false', 'message' => $message, 'status' => 400 );
        }
    }

    private function decipherToken( array $authHeader )
    {
        if ($authHeader) {

            list($jwt) = sscanf( $authHeader[0], 'Bearer %s');
            if ($jwt) {
                try {
                    $config = include(__DIR__.'/../../minerva_config.php');

                    //decode the jwt using the key from config
                    $secretKey = base64_decode($config['jwtKey']);
                    $token = JWT::decode($jwt, $secretKey, array('HS512'));
                    return $token;

                } catch (Exception $e) {
                    LogHelper::getInstance()->log( $e );
                    return null;
                }
            } else {
                $message = "No token was able to be extracted from the authorization header.";
                LogHelper::getInstance()->log( $message );
                return null;
            }
        } else {
            $message = "Token not found in request";
            LogHelper::getInstance()->log( $message );
            return null;
        }
    }

    public function grantAdminAccess( array $authHeader ) : int
    {
        $security = $this->grantBasicAccess( $authHeader );
        if( $security === HttpStatus::Unauthorized){
            return $security;
        }
        else{
            $user = $this->getUser( $authHeader );
            if( $user->getType() !== UserTypes::Admin ){
                return HttpStatus::Unauthorized ;
            }
            else{
                if( ! $user->getApproved()){
                    return HttpStatus::Unauthorized;
                }
            }
        }

        return HttpStatus::OK;
    }

    public function  grantUserAccess( array $authHeader ) : int
    {
        $security = $this->grantBasicAccess( $authHeader );
        if( $security === HttpStatus::Unauthorized ){
            return HttpStatus::Unauthorized;
        }
        else{
            $user = $this->getUser( $authHeader );
            if( $user->getType() === UserTypes::Admin){
                LogHelper::getInstance()
                ->log("Access denied! You are not a common user!");
                return HttpStatus::Unauthorized;
            }
            else{
                return HttpStatus::OK;
            }
        }
    }

    public function  grantStudentAccess( array $authHeader ) : int
    {
        $security = $this->grantBasicAccess( $authHeader );
        if( $security === HttpStatus::Unauthorized ){
            return HttpStatus::Unauthorized;
        }
        else{
            $user = $this->getUser( $authHeader );
            if( $user->getType() !== UserTypes::Student ){
                LogHelper::getInstance()
                    ->log("Access denied! You are not a student user!");
                return HttpStatus::Unauthorized;
            }
            else{
                return HttpStatus::OK;
            }
        }
    }


    public function grantBasicAccess( array $authHeader) :int
    {
        if( empty( $authHeader) ){
            return HttpStatus::Unauthorized;
        }

        $token = $this->decipherToken( $authHeader );

        if( $token === null ){
            return HttpStatus::Unauthorized;
        }

        $user = CollectionHelper::getInstance()
            ->find('User', $token->data->id);
        if( $user === null ){
            return HttpStatus::Unauthorized;
        }

        if( ! $user->getApproved() ){
            return HttpStatus::Unauthorized;
        }

        return HttpStatus::OK;
    }

    public function getUser( array $authHeader )
    {
        $token = $this->decipherToken( $authHeader );
        return CollectionHelper::getInstance()
            ->find("User", $token->data->id);
    }

    public function evaluateAddmissions() : int{
        $config = include(__DIR__.'/../../minerva_config.php');
        if( ! $config['debug'] ){
            $filter = array("name" => DataConstants::conf_admissions);
            $conf = CollectionHelper::getInstance()
                ->findByFilter("Conf",$filter);
            $conf = $conf[0];

            if( ! $conf->getStatus() ){
                return HttpStatus::Not_Acceptable;
            }
        }
        return HttpStatus::OK;
    }
}