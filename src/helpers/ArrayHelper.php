<?php

class ArrayHelper
{
    public function retrieveValueOrNull(string $key,$arr){

        if($arr === null){
            return null;
        }
        if(array_key_exists($key, $arr)){
            return $arr[$key];
        }
        else{
            return null;
        }
    }

    public function castSringToInt(array $arr) : array {
        foreach ($arr as $key => $value){
            $arr[$key] = (int)$value;
        }
        return $arr;
    }
}