<?php

class CollectionHelper
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new CollectionHelper();
        }
        return self::$instance;
    }

    public function existsByFilter(string $targetEntity, array $filter) : bool
    {
        $arr = $GLOBALS['em']->getRepository($targetEntity)->findBy($filter);

        $size = count( $arr );
        if( $size === 1 ){
            $arr = $arr[0];
            return (bool)$arr;
        }

        if( empty($arr) ){
            return false;
        }

        if( $size > 1 ){
            return true;
        }
        return (bool)$arr;
    }

    public function findByFilter( string $targetEntity, $filter ){

        if( $filter === null ){
            return self::findAll($targetEntity);
        }

        $arr = $GLOBALS['em']->getRepository($targetEntity)->findBy($filter);

        if( empty($arr) ){
            return null;
        }

        return $arr;
    }

    public function select( array $entities, array $select )
    {
        $entities = json_decode( json_encode(  $entities ) );
        $result = Array();

        foreach( $entities as $entity){
            $arr = array();
            foreach( $entity as $key => $value ) {
                if (in_array($key, $select)) {
                    $arr = array_merge( $arr, array($key => $value) );
                }
            }
            $result[] = $arr;
        }

        return $result;
    }

    public function return_ids(array $entities) : array {
        $ids = array();
        foreach ($entities as $entity){
            array_push($ids, $entity->getId());
        }
        return $ids;
    }

    public function findAll( string $targetEntity){

        return $GLOBALS['em']->getRepository($targetEntity)->findAll();
    }

    public function find( string $targetEntity, int $id)
    {
        return $GLOBALS['em']->find( $targetEntity , $id);
    }

    public function exists( string $targetEntity)
    {
        $arr = $GLOBALS['em']->getRepository($targetEntity)->findAll();
        return !empty($arr);
    }
}