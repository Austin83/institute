<?php

abstract class HttpStatus
{
    const OK = 200;
    const Bad_Request = 400;
    const Unauthorized = 401;
    const Not_Found= 404;
    const Not_Acceptable = 406;
    const Conflict = 409;
}