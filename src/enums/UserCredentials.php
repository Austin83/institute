<?php

abstract class UserCredentials
{
    const Username = 0;
    const Email = 1;
    const Document = 2;
}