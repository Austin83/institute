<?php

abstract class UserTypes
{
    const Admin = 0;
    const Teacher = 1;
    const Student = 2;
}