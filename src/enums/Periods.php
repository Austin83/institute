<?php

abstract class Periods
{
    const First = 0;
    const Second = 1;
    const Third = 2;
    const Fourth = 3;
    const Fifth = 4;
    const Sixth = 5;
    const Seventh = 6;
}