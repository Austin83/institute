<?php

abstract class DataConstants
{
    const Document = 8;
    const Username = 20;
    const Pass_min = 8;
    const phone_min = 6;
    const conf_admissions = "admissions";
    const school_year = array("-03-01 00:00:00","-11-30 23:59:59");
    const period_bounds = array(0,6);
    const weekdays_bounds = array(1,6);
    const week_length = 7;
    const shifts = 3;
}