<?php

/**
 * @Entity @Table(name="configs")
 **/
class Conf
{
    /**
     * @Id @Column(type="string", unique=true)
     **/
    private $name;

    /**
     * @Column(type="boolean")
     **/
    private $status;

    public function __construct($name,$status){
        $this->name = $name;
        $this->status = $status;
        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}