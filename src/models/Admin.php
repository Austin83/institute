<?php

/**
 * @Entity @Table(name="admins")
 **/
class Admin extends User
{

    public function __construct( array $data ){
        parent::__construct( $data );
        $this->approved = true;
    }

    public function getType() : int
    {
        return UserTypes::Admin;
    }

    public function update( array $data )
    {
        if( $data['user']['document'] !== null ){
            $this->document = $data['user']['document'];
        }

        if( $data['user']['password'] !== null ){
            $this->password = $data['user']['password'];
        }

        if( $data['user']['email'] !== null ){
            $this->email = $data['user']['email'];
        }


        if( $data['user']['name'] !== null){
            $this->name = $data['user']['name'];
        }

        if( $data['user']['birthday'] !== null ){
            $this->birthday = DateTime::createFromFormat('d/m/Y', $data['user']['birthday']);
        }

        if( $data['user']['registeredSince'] !== null ){
            $this->registeredSince =
                DateTime::createFromFormat('d/m/Y', $data['user']['registeredSince']);
        }

        if( $data['user']["add"]['phones'] !== null ){
            $this->phones = array_unique(array_merge( $this->phones, $data['user']["add"]['phones']));
        }

        if( $data['user']["remove"]['phones'] !== null ){
            $this->phones =  \array_diff($this->phones, $data['user']["remove"]['phones']);
        }
    }

    public function getInfo( InfoHelper $filter_admin ) : array
    {
        if( $filter_admin === null || empty($filter_admin->getFilter()) )
        {
            return $this->getInfoAll();
        }

        $info = null;

        switch ( $filter_admin->getMode() )
        {
            case GetInfoModes::Optimistic:
                $info = new StdClass();
                if( in_array(SelectAdmin::id, $filter_admin->getFilter()) )
                {
                    $info->id = $this->id;
                }
                if( in_array( SelectAdmin::username, $filter_admin->getFilter()) )
                {
                    $info->username = $this->username;
                }
                if( in_array( SelectAdmin::document, $filter_admin->getFilter()) )
                {
                    $info->document = $this->document;
                }
                if( in_array( SelectAdmin::name, $filter_admin->getFilter()) )
                {
                    $info->name = $this->name;
                }
                if( in_array( SelectAdmin::email, $filter_admin->getFilter()) )
                {
                    $info->email = $this->email;
                }
                if( in_array( SelectAdmin::birthday, $filter_admin->getFilter()) )
                {
                    $info->birthday = $this->birthday->format('d/m/Y');
                }
                if( in_array( SelectAdmin::registeredSince, $filter_admin->getFilter()) )
                {
                    $info->registeredSince = $this
                        ->registeredSince->format('d/m/Y H:i:s');
                }
                if( in_array( SelectAdmin::phones, $filter_admin->getFilter()) )
                {
                    $info->phones = $this->phones;
                }
                return json_decode(json_encode($info), true);

            case GetInfoModes::Pesimistic:
                $info = $this->getInfoAll();
                if( in_array(SelectAdmin::id, $filter_admin->getFilter()) )
                {
                    unset($info["id"]);
                }
                if( in_array( SelectAdmin::username, $filter_admin->getFilter()) )
                {
                    unset($info["username"]);
                }
                if( in_array( SelectAdmin::document, $filter_admin->getFilter()) )
                {
                    unset($info["document"]);
                }
                if( in_array( SelectAdmin::name, $filter_admin->getFilter()) )
                {
                    unset($info["name"]);
                }
                if( in_array( SelectAdmin::email, $filter_admin->getFilter()) )
                {
                    unset($info["email"]);
                }
                if( in_array( SelectAdmin::birthday, $filter_admin->getFilter()) )
                {
                    unset($info["birthday"]);
                }
                if( in_array( SelectAdmin::registeredSince, $filter_admin->getFilter()) )
                {
                    unset($info["registeredSince"]);
                }
                if( in_array( SelectAdmin::phones, $filter_admin->getFilter()) )
                {
                    unset($info["phones"]);
                }

                return $info;
        }
    }

    public function getInfoAll() : array
    {
        $info = new StdClass();
        $info->id = $this->id;
        $info->username = $this->username;
        $info->document = $this->document;
        $info->name = $this->name;
        $info->email = $this->email;
        $info->birthday = $this->birthday->format('d/m/Y');
        $info->registeredSince = $this
            ->registeredSince->format('d/m/Y H:i:s');
        $info->phones = $this->phones;
        return json_decode(json_encode($info), true);
    }
}