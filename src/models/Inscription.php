<?php

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="inscriptions")
 **/

class Inscription {

    /** @Id @Column(type="integer") @GeneratedValue **/
    private $id;
    /**
     * @ManyToOne(targetEntity="Student")
     * @JoinColumn(name="student", referencedColumnName="id")
     **/
    private $student;

    /**
     * @ManyToOne(targetEntity="Subject")
     * @JoinColumn(name="subject", referencedColumnName="id")
     **/
    private $subject;

    /**
     * @Column(type="datetime")
     **/
    private $inscribedSince;

    public function __construct(Student $student, Subject $subject){

        $this->student = $student;
        $this->subject = $subject;
        $timeZone = 'America/Montevideo';
        $date = new DateTime("now", new DateTimeZone($timeZone) );
        $this->inscribedSince = $date;
        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Student
     */
    public function getStudent(): Student
    {
        return $this->student;
    }

    /**
     * @return Subject
     */
    public function getSubject(): Subject
    {
        return $this->subject;
    }

    public function getInscribedSince()
    {
        return $this->inscribedSince;
    }

    /**
     * @param DateTime $inscribedSince
     */
    public function setInscribedSince(DateTime $inscribedSince)
    {
        $this->inscribedSince = $inscribedSince;
    }
}