<?php

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="topics")
 **/
class Topic
{
    /** @Id @Column(type="integer") @GeneratedValue(strategy="AUTO") **/
    private $id;
    /** @Column(type="string") */
    private $name;

    /**
     * @ManyToOne(targetEntity="Subject")
     */
    private $subject;

    /**
     * @ManyToOne(targetEntity="Topic", cascade={"persist", "remove"})
     **/
    private $parent;

    public function __construct( array $topic, Subject $subject){

        $this->name = $topic['name'];
        $this->subject = $subject;
        if( $topic['children'] !== null ){

            foreach ( $topic['children'] as $child) {
                $child = new Topic( $child, $subject);
                $child->setParent($this);
                $GLOBALS['em']->persist($child);
            }
        }
        $GLOBALS['em']->persist($this);
    }

    public function getInfo(InfoHelper $filter_topic) : array {

        if( $filter_topic === null || empty($filter_topic->getFilter()) )
        {
            return $this->getInfoAll();
        }

        $info = new StdClass();
        switch ( $filter_topic->getMode() )
        {
            case GetInfoModes::Optimistic:

                if( in_array(SelectTopic::id, $filter_topic->getFilter()))
                {
                    $info->id = $this->id;
                }
                if( in_array( SelectTopic::name, $filter_topic->getFilter()))
                {
                    $info->name = $this->name;
                }
                if( in_array( SelectTopic::subject_id, $filter_topic->getFilter()))
                {
                    $info->subject_id = $this->subject->getId();
                }
                if( in_array(SelectTopic::parent_id, $filter_topic->getFilter())){
                    $info->parent_id = $this->parent->getId();
                }
                break;

            case GetInfoModes::Pesimistic:

                if(!in_array(SelectTopic::id, $filter_topic->getFilter()))
                {
                    $info->id = $this->id;
                }
                if(!in_array( SelectTopic::name, $filter_topic->getFilter()))
                {
                    $info->name = $this->name;
                }
                if(!in_array( SelectTopic::subject_id, $filter_topic->getFilter()))
                {
                    $info->subject_id = $this->subject->getId();
                }
                if(!in_array(SelectTopic::parent_id, $filter_topic->getFilter())){
                    if( $this->parent !== null ){
                        $info->parent_id = $this->parent->getId();
                    }
                }
                break;
        }
        return json_decode(json_encode($info), true);
    }

    public function getInfoAll() : array {
        $info = new StdClass();
        $info->id = $this->id;
        $info->name = $this->name;
        if( $this->parent !== null ){
            $info->parent_id = $this->parent->getId();
        }
        return json_decode(json_encode($info), true);
    }

    public function update( array $data ){
        $this->name = $data['name'];
    }

    /**
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParent() : Topic
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

}