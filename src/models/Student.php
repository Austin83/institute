<?php

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="students")
 **/
class Student extends User
{

    /**
     * @OneToMany(targetEntity="Inscription", mappedBy="student")
     * */
    private $inscriptions;

    public function __construct( array $data ){
        parent::__construct( $data );
        $this->inscriptions = new ArrayCollection();
        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    public function getType() : int
    {
        return UserTypes::Student;
    }

    public function updateByStudent( array $data )
    {
        if( $data['password'] !== null ){
            $this->password = password_hash( $data['password'], PASSWORD_DEFAULT);
        }

        if( $data['email'] !== null ){
            $this->email = $data['email'];
        }

        if( $data["add"]['phones'] !== null ){
            $this
                ->phones
                = array_unique(array_merge( $this->phones, $data["add"]['phones']));
        }

        if( $data["remove"]['phones'] !== null ){
            $this->phones =  \array_diff($this->phones, $data["remove"]['phones']);
        }
    }

    public function updateByAdmin( array $data )
    {
        if( $data['user']['document'] !== null ){
            $this->document = $data['user']['document'];
        }

        if( $data['user']['password'] !== null ){
            $this->password = password_hash( $data['user']['password'], PASSWORD_DEFAULT);
        }

        if( $data['user']['email'] !== null ){
            $this->email = $data['user']['email'];
        }


        if( $data['user']['name'] !== null){
            $this->name = $data['user']['name'];
        }

        if( $data['user']['birthday'] !== null ){
            $this->birthday = DateTime::createFromFormat('d/m/Y', $data['user']['birthday']);
        }

        if( $data['user']['registeredSince'] !== null ){
            $this->registeredSince =
                DateTime::createFromFormat('d/m/Y', $data['user']['registeredSince']);
        }

        if( $data['user']["add"]['phones'] !== null ){
            $this->phones = array_unique(array_merge( $this->phones, $data['user']["add"]['phones']));
        }

        if( $data['user']["remove"]['phones'] !== null ){
            $this->phones =  \array_diff($this->phones, $data['user']["remove"]['phones']);
        }
    }

    /**
     * @return Collection
     */
    public function getInscriptions(): Collection
    {
        return $this->inscriptions;
    }

    /**
     * @param mixed $inscriptions
     */
    public function setInscriptions( ArrayCollection $inscriptions)
    {
        $this->inscriptions = $inscriptions;
    }

    public function addInscription( Inscription $inscription ){
        $this->inscriptions->add( $inscription );
    }

    public function getInfo( InfoHelper $filter_student ) : array
    {
        if( $filter_student === null || empty($filter_student->getFilter()) )
        {
            return $this->getInfoAll();
        }

        $info = new StdClass();

        switch ( $filter_student->getMode() )
        {
            case GetInfoModes::Optimistic:
                if( in_array(SelectStudent::id, $filter_student->getFilter()) )
                {
                    $info->id = $this->id;
                }
                if( in_array( SelectStudent::username, $filter_student->getFilter()) )
                {
                    $info->username = $this->username;
                }
                if( in_array( SelectStudent::document, $filter_student->getFilter()) )
                {
                    $info->document = $this->document;
                }
                if( in_array( SelectStudent::name, $filter_student->getFilter()) )
                {
                    $info->name = $this->name;
                }
                if( in_array( SelectStudent::email, $filter_student->getFilter()) )
                {
                    $info->email = $this->email;
                }
                if( in_array( SelectStudent::birthday, $filter_student->getFilter()) )
                {
                    $info->birthday = $this->birthday;
                }
                if( in_array( SelectStudent::registeredSince, $filter_student->getFilter()) )
                {
                    $info->registeredSince = $this->registeredSince;
                }
                if( in_array( SelectStudent::phones, $filter_student->getFilter()) )
                {
                    $info->phones = $this->phones;
                }
                break;
            case GetInfoModes::Pesimistic:
                if( in_array(SelectStudent::id, $filter_student->getFilter()) )
                {
                    $info->id = $this->id;
                }
                if( in_array( SelectStudent::username, $filter_student->getFilter()) )
                {
                    $info->username = $this->username;
                }
                if( in_array( SelectStudent::document, $filter_student->getFilter()) )
                {
                    $info->document = $this->document;
                }
                if( in_array( SelectStudent::name, $filter_student->getFilter()) )
                {
                    $info->name = $this->name;
                }
                if( in_array( SelectStudent::email, $filter_student->getFilter()) )
                {
                    $info->email = $this->email;
                }
                if( in_array( SelectStudent::birthday, $filter_student->getFilter()) )
                {
                    $info->birthday = $this->birthday;
                }
                if( in_array( SelectStudent::registeredSince, $filter_student->getFilter()) )
                {
                    $info->registeredSince = $this->registeredSince;
                }
                if( in_array( SelectStudent::phones, $filter_student->getFilter()) )
                {
                    $info->phones = $this->phones;
                }
        }
        return json_decode(json_encode($info), true);
    }

    public function getInfoAll() : array
    {
        $info = new StdClass();
        $info->type = $this->getType();
        $info->id = $this->id;
        $info->document = $this->document;
        $info->username = $this->username;
        $info->email = $this->email;
        $info->name = $this->name;
        $info->surname = $this->surname;
        $info->approved = $this->approved;
        $info->birthday = $this->birthday;
        $info->phones = $this->phones;
        $info->registeredSince = $this->registeredSince;
        return json_decode(json_encode($info), true);
    }
}