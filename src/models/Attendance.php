<?php
/**
 * Created by PhpStorm.
 * User: sebastianndiaz
 * Date: 24/03/19
 * Time: 04:39 PM
 */

/**
 * @Entity @Table(name="attendances")
 **/

class Attendance {

    /** @Id @Column(type="integer") @GeneratedValue(strategy="AUTO") **/
    private $id;

    /**
     * @ManyToOne(targetEntity="Student")
     * @JoinColumn(name="student", referencedColumnName="id")
     **/
    private $student;

    /**
     * @ManyToOne(targetEntity="Lesson")
     * @JoinColumn(name="lesson", referencedColumnName="id")
     **/
    private $lesson;

    public function __construct(Student $student, int $id_lesson) {
        $this->student = $student;
        $this->lesson = CollectionHelper::getInstance()
            ->find("Lesson", $id_lesson);
        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Student
     */
    public function getStudent(): Student {
        return $this->student;
    }

    /**
     * @return Lesson
     */
    public function getLesson(): Lesson {
        return $this->lesson;
    }

}