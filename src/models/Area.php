<?php
/**
 * Created by PhpStorm.
 * User: sebastianndiaz
 * Date: 24/03/19
 * Time: 04:45 PM
 */

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="areas")
 **/

class Area {

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    private $id;

    /**
     * @Column(type="string")
     **/
    private $name;

    /**
     * @ManyToOne(targetEntity="Teacher")
     * @JoinColumn(name="teacher_id", referencedColumnName="id")
     **/
    private $teacher;

    public function __construct( array $data) {
        $this->name = $data["name"];
        if( array_key_exists("id_teacher",$data) ){
            if( $data["id_teacher"] === null ) {
                $this->teacher = null;
            }else{
                $this->teacher = CollectionHelper::getInstance()
                    ->find("Teacher", $data["id_teacher"]);
            }
        }

        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    public function update( array $data){

        if( array_key_exists("name", $data) && $this->name !== $data["name"]){
            $this->name = $data["name"];
        }
        if( array_key_exists("id_teacher", $data) ){
            if( $data["id_teacher"] === null){
                $this->teacher = null;
            }
            else{
                if( $this->teacher !== null){
                    if( $data["id_teacher"] !== $this->teacher->getId()){
                        $this->teacher = CollectionHelper::getInstance()
                            ->find("Teacher", $data["id_teacher"]);
                    }
                }
                else{
                    $this->teacher = CollectionHelper::getInstance()
                        ->find("Teacher", $data["id_teacher"]);
                }
            }
        }
        $GLOBALS["em"]->persist($this);
        $GLOBALS["em"]->flush();
    }

    public function getInfo( InfoHelper $filter_area ): array
    {
        if( $filter_area === null || empty($filter_area->getFilter()) )
        {
            return $this->getInfoAll();
        }

        $info = new StdClass();
        switch ( $filter_area->getMode() )
        {
            case GetInfoModes::Optimistic:

                if( in_array(SelectArea::id, $filter_area->getFilter()))
                {
                    $info->id = $this->id;
                }
                if( in_array( SelectArea::name, $filter_area->getFilter()))
                {
                    $info->name = $this->name;
                }
                if( in_array( SelectArea::teacher_id, $filter_area->getFilter()))
                {
                    if( $this->teacher !== null ){
                        $info->id_teacher = $this->teacher->getId();
                    }
                }
                break;

            case GetInfoModes::Pesimistic:

                if( ! in_array(SelectArea::id, $filter_area->getFilter()))
                {
                    $info->id = $this->id;
                }
                if( !in_array( SelectArea::name, $filter_area->getFilter()))
                {
                    $info->name = $this->name;
                }
                if( !in_array( SelectArea::teacher_id, $filter_area->getFilter()))
                {
                    if( $this->teacher !== null ){
                        $info->id_teacher = $this->teacher->getId();
                    }
                }
                break;
        }
        return json_decode(json_encode($info), true);
    }

    public function getInfoAll() : array
    {
        $info = new StdClass();
        $info->id = $this->id;
        $info->name = $this->name;
        return json_decode(json_encode($info), true);
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * @return Teacher
     */
    public function getTeacher(): Teacher {
        return $this->teacher;
    }

    /**
     * @param Teacher $teacher
     */
    public function setTeacher(Teacher $teacher) {
        $this->teacher = $teacher;
    }
}