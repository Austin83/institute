<?php

/**
 * @Entity @Table(name="classrooms")
 **/

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class Classroom {

    public function __construct( array $data )
    {
        $this->name = $data['name'];
        $this->capacity = $data['capacity'];
        $this->floor = $data['floor'];
        $this->preferences = new ArrayCollection();
        if( array_key_exists("preferences",$data) ){
            $filter = array( "id" => $data["preferences"]);
            $aux = CollectionHelper::getInstance()
                ->findByFilter("Subject",$filter);
            foreach ($aux as $preference ){
                $this->preferences->add($preference);
            }
        }

        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    public function update( array $data ){
        if( array_key_exists("name", $data)){
            $this->name = $data['name'];
        }
        if( array_key_exists("capacity", $data)){
            $this->capacity = $data['capacity'];
        }
        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    public function getInfoAll() : array {
        $info = new StdClass();
        $info->id = $this->id;
        $info->name = $this->name;
        $info->capacity = $this->capacity;
        $info->floor = $this->floor;

        if( ! $this->preferences->isEmpty()){
            $info->preferences = array();
            foreach($this->preferences as $preference){
                array_push($info->preferences, $preference->getId());
            }
        }


        return json_decode(json_encode($info), true);
    }

    public function isPreferred( int $id_subject ) : bool {
        if( $this->preferences->isEmpty() ){
            return false;
        }

        foreach ( $this->preferences as $pref ){
            if( $pref->getId() === $id_subject ){
                return true;
            }
        }
        return false;
    }

    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    private $id;

    /**
     * @Column(type="string")
     **/
    private $name;

    /**
     * @Column(type="integer")
     **/
    private $capacity;

    /**
     * @Column(type="integer")
     **/
    private $floor;

    /**
     * @ManyToMany(targetEntity="Subject")
     * @JoinTable(name="classroom_preferences")
     */
    private $preferences;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name) {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCapacity(): int {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     */
    public function setCapacity(int $capacity) {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getFloor(): int {
        return $this->floor;
    }

    /**
     * @param int $floor
     */
    public function setFloor(int $floor) {
        $this->floor = $floor;
    }

    /**
     * @return mixed
     */
    public function getPreferences() : Collection
    {
        return $this->preferences;
    }
}