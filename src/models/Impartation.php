<?php
/**
 * Created by PhpStorm.
 * User: sebastianndiaz
 * Date: 24/03/19
 * Time: 04:18 PM
 */

/**
 * @Entity @Table(name="impartations")
 **/

class Impartation {

    /**
     * @Id @OneToOne(targetEntity="Teacher")
     * @JoinColumn(name="teacher", referencedColumnName="id")
     **/
    private $teacher;

    /**
     * @Id @OneToOne(targetEntity="Lesson")
     * @JoinColumn(name="lesson", referencedColumnName="id")
     **/
    private $lesson;

    /**
     * @ManyToOne(targetEntity="Subject")
     * @JoinColumn(name="subject", referencedColumnName="id")
     **/
    private $subject;

    public function __construct(int $id_teacher,Lesson $lesson, int $id_subject)
    {
        $this->teacher = CollectionHelper::getInstance()
            ->find("Teacher",$id_teacher);
        $this->lesson = $lesson;
        $this->subject = CollectionHelper::getInstance()
            ->find("Subject", $id_subject);
        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    /**
     * @return Teacher
     */
    public function getTeacher(): Teacher {
        return $this->teacher;
    }

    /**
     * @return Lesson
     */
    public function getLesson(): Lesson {
        return $this->lesson;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }
}