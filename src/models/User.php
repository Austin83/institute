<?php

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity @Table(name="users")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discr", type="string")
 * @DiscriminatorMap({"user" = "User", "teacher" = "Teacher", "student" = "Student",
 *                      "admin" = "Admin"})
 **/
class User
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;

    /** @Column(type="string",length=8) */
    protected $document;

    /** @Column(type="string",length=20) */
    protected $username;

    /** @Column(type="string") */
    protected $password;

    /** @Column(type="string") */
    protected $email;

    /** @Column(type="string") */
    protected $name;

    /** @Column(type="string") */
    protected $surname;

    /** @Column(type="boolean") */
    protected $approved;

    /**
     * @var
     */
    /** @Column(type="date") */
    protected $birthday;
    /**
     * @var
     */
    /** @Column(type="array") */
    protected $phones;

    /** @Column(type="date") */
    protected $registeredSince;

    public function __construct( array $data ) {
        $this->phones = array();
        $this->document = $data['document'];
        $this->username = $data['username'];
        $this->password = password_hash( $data['password'], PASSWORD_DEFAULT);
        $this->email = $data['email'];
        $this->name = $data['name'];
        $this->surname = $data['surname'];
        $this->birthday = DateTime::createFromFormat('Y-m-d', $data['birthday']);
        $dt = new DateTime();
        $this->registeredSince
            = DateTime::createFromFormat('Y-m-d', $dt->format("Y-m-d"));
        $this->phones = array_unique($data['phones']);
        $this->approved = false;
    }

    public function getType() : int
    {
        return -1;//Flag invalid
    }

    /**
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDocument() : string
    {
        return $this->document;
    }

    /**
     * @param mixed $document
     */
    public function setDocument(string $document)
    {
        $this->document = $document;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * @param mixed $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }


    /**
     * @return mixed
     */
    public function getBirthday() : DateTime
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday(DateTime $birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return mixed
     */
    public function getPhones() : array
    {
        return $this->phones;
    }

    /**
     * @param mixed $phones
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;
    }

    /**
     * @return mixed
     */
    public function getRegisteredSince() : DateTime
    {
        return $this->registeredSince;
    }

    /**
     * @param mixed $registerSince
     */
    public function setRegisteredSince(DateTime $registerSince)
    {
        $this->registeredSince = $registerSince;
    }
}