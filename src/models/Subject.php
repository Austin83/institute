<?php

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @Entity @Table(name="subjects")
 **/
class Subject
{
    /** @Id @Column(type="integer") @GeneratedValue(strategy="AUTO") **/
    private $id;
    /** @Column(type="string") **/
    private $name;

    /** @ManyToOne(targetEntity="Area", cascade={"persist"})
     **/
    private $area;

    /**
     * @OneToMany(targetEntity="Inscription", mappedBy="inscriptions")
     * */
    private $inscriptions;

    /**
     * @ManyToMany(targetEntity="Subject", cascade={"remove"})
     * @JoinTable(name="priors",
     *      joinColumns={@JoinColumn(name="source_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="prior_id", referencedColumnName="id")}
     *      )
     */
    private $priors;

    public function __construct( array $data){
        $this->name = $data['name'];
        $this->priors = new ArrayCollection();
        $this->inscriptions = new ArrayCollection();
        $this->area = CollectionHelper::getInstance()
            ->find("Area", $data['area_id']);
        $GLOBALS['em']->persist($this);
        foreach ( $data['topics'] as $topic ){
            new Topic($topic, $this);
        }
        if( array_key_exists("prior_ids", $data)){
            $this->setPriorIds($data['prior_ids']);
        }
        $GLOBALS['em']->flush();
    }

    public function update( array  $data){
        if( array_key_exists("name", $data) ){
            $this->name = $data["name"];
            $GLOBALS["em"]->persist($this);
        }
        $connection = $GLOBALS["em"]->getConnection();
        if( array_key_exists("priors", $data) ){
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            foreach ($this->priors->toArray() as $p){
                $this->priors->removeElement($p);
            }
            if( ! empty($data["priors"]) ){
                foreach ($data["priors"] as $prior){
                    $this->priors->add(CollectionHelper::getInstance()
                        ->find("Subject", $prior));
                }
            }
        }

        if( array_key_exists("topics", $data)){
            $filter = array( "subject" => $this );
            $topics = CollectionHelper::getInstance()
                ->findByFilter("Topic", $filter);
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            foreach ($topics as $topic){
                $GLOBALS["em"]->remove($topic);
            }
            foreach ($data["topics"] as $topic){
                new Topic($topic, $this);
            }
        }
        $GLOBALS["em"]->flush();
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
    }

    private function setPriorIds( array $prior_ids){
        if( $prior_ids !== null || !empty($prior_ids)){
            foreach( $prior_ids as $id ){
                $prior = CollectionHelper::getInstance()
                    ->find("Subject", $id );
                if( $prior !== null ){
                    $this->priors->add( $prior );
                }
            }
        }
    }

    public function getInfo( InfoHelper $filter_subject,
                             InfoHelper $filter_topic = null,
                             InfoHelper $filter_area = null) : array
    {
        if( $filter_subject === null )
        {
            return $this->getInfoAll();
        }

        $info = new StdClass();
        switch ( $filter_subject->getMode() )
        {
            case GetInfoModes::Optimistic:

                if( in_array(SelectSubject::id, $filter_subject->getFilter()))
                {
                    $info->id = $this->id;
                }
                if( in_array( SelectSubject::name, $filter_subject->getFilter()))
                {
                    $info->name = $this->name;
                }
                if( in_array( SelectSubject::topics, $filter_subject->getFilter()))
                {
                    $topics_info = array();
                    $criteria = array( "subject" => $this );
                    $topics = CollectionHelper::getInstance()
                        ->findByFilter("Topic", $criteria );
                    if( $filter_topic === null ){
                        foreach ($topics as $topic){
                            array_push($topics_info, $topic->getInfoAll());
                        }
                    }
                    else{
                        foreach ($topics as $topic){
                            array_push($topics_info, $topic->getInfo($filter_topic));
                        }
                    }

                    $info->topics = $topics_info;
                }
                if( in_array( SelectSubject::area, $filter_subject->getFilter()))
                {
                    if( $filter_area === null ){
                        $info->area = $this->area->getInfo($filter_area);
                    }else{
                        $info->area = $this->area->getInfoAll();
                    }
                }
                if( in_array(SelectSubject::priors, $filter_subject->getFilter())){
                    if( ! $this->priors->isEmpty() ){
                        $info->priors = array();
                        foreach ($this->priors as $prior){
                            array_push($info->priors, $prior->getId());
                        }
                    }
                }
                break;

            case GetInfoModes::Pesimistic:

                if( ! in_array(SelectSubject::id, $filter_subject->getFilter()))
                {
                    $info->id = $this->id;
                }
                if( !in_array( SelectSubject::name, $filter_subject->getFilter()))
                {
                    $info->name = $this->name;
                }
                if(!in_array( SelectSubject::topics, $filter_subject->getFilter()))
                {
                    $topics_info = array();
                    $criteria = array( "subject" => $this );
                    $topics = CollectionHelper::getInstance()
                        ->findByFilter("Topic", $criteria );
                    if( $filter_topic === null ){
                        foreach ($topics as $topic){
                            array_push($topics_info, $topic->getInfoAll());
                        }
                    }
                    else{
                        if( ! empty($topics) ){
                            foreach ($topics as $topic){
                                array_push($topics_info, $topic->getInfo($filter_topic));
                            }
                        }
                    }

                    $info->topics = $topics_info;
                }
                if( !in_array( SelectSubject::area, $filter_subject->getFilter()))
                {
                    if( $filter_area !== null ){
                        $info->area = $this->area->getInfo($filter_area);
                    }else{
                        $info->area = $this->area->getInfoAll();
                    }
                }
                if( ! in_array(SelectSubject::priors, $filter_subject->getFilter())){
                    if( ! $this->priors->isEmpty() ){
                        $info->priors = array();
                        foreach ($this->priors as $prior){
                            array_push($info->priors, $prior->getId());
                        }
                    }
                }
                break;
        }
        return json_decode(json_encode($info), true);
    }

    public function getInfoAll() : array
    {
        $info = new StdClass();
        $info->id = $this->id;
        $info->name = $this->name;
        $info->area = new StdClass();
        $info->area->id = $this->area->getId();
        $info->area->name = $this->area->getName();
        $filter = array( "subject" => $this );
        $topics = CollectionHelper::getInstance()
            ->findByFilter("Topic", $filter );
        $topics_info = array();
        if( is_array($topics) && !empty($topics) ){
            foreach ($topics as $topic){
                array_push($topics_info, $topic->getInfoAll());
            }
        }
        $info->topics = $topics_info;
        return json_decode(json_encode($info), true);
    }

    /**
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getInscriptions() : ArrayCollection
    {
        return $this->inscriptions;
    }

    /**
     * @param mixed $inscriptions
     */
    public function setInscriptions( ArrayCollection $inscriptions)
    {
        $this->inscriptions = $inscriptions;
    }

    /**
     * @return mixed
     */
    public function getPriors() : ArrayCollection
    {
        return $this->priors;
    }

    /**
     * @param mixed $priors
     */
    public function setPriors( ArrayCollection $priors)
    {
        $this->priors = $priors;
    }

    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getArea() : Area
    {
        return $this->area;
    }

    /**
     * @param mixed $area
     */
    public function setArea(Area $area)
    {
        $this->area = $area;
    }

    public function addInscription( Inscription $inscription ){
        $this->inscriptions->add( $inscription );
    }

}