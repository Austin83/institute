<?php

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="lessons")
 **/
class Lesson {

    /** @Id @Column(type="integer") @GeneratedValue(strategy="AUTO") **/
    private $id;
    /**
     * @ManyToOne(targetEntity="Classroom")
     */
    private $classroom;

    /**
     * @Column(type="integer")
     */
    private $period_start;

    /**
     * @Column(type="integer")
     */
    private $period_end;

    /**
     * @Column(type="integer")
     */
    private $shift;

    /**
     * @ManyToOne(targetEntity="Teacher")
     */
    private $teacher;
    /**
     * @ManyToOne(targetEntity="Subject")
     */
    private $subject;

    /** @Column(type="date") */
    private $date;

    /** @Column(type="integer") */
    private $weekday;

    //public function __construct(array $data, int $shift, int $weekday,string $stamp) {
    public function __construct(array $data, array $data_aux, int $id_subject) {

        $this->classroom = CollectionHelper::getInstance()
            ->find("Classroom",$data["id_classroom"][$data_aux["shift"]]);
        $this->teacher = CollectionHelper::getInstance()
            ->find("Teacher", $data["id_teacher"][$data_aux["shift"]]);
        $this->subject = CollectionHelper::getInstance()
            ->find("Subject", $id_subject);
        if( is_array($data["periods"]) ){
            $this->period_start = $data["periods"][0];
            $this->period_end = $data["periods"][1];
        }
        else{
            $this->period_start = $data["periods"];
            $this->period_end = $data["periods"];
        }
        $this->shift = $data_aux["shift"];
        $this->date = DateTime::createFromFormat("Y-m-d", $data_aux["stamp"]);
        $this->weekday = $data_aux["day"];
        $GLOBALS['em']->persist($this);
    }

    public function overlaps( int $start, int $end) : int {

        if( ( $this->period_end < $start) || ($this->period_start > $end)){
            return HttpStatus::OK;
        }

        return HttpStatus::Conflict;
    }

    public function getInfoAll(User $user = null) : array
    {
        $info = new StdClass();
        $info->id = $this->id;
        $info->id_classroom = $this->classroom->getId();
        $info->id_teacher = $this->teacher->getId();
        $info->id_subject = $this->subject->getId();
        $info->periods = array($this->period_start,
                                $this->period_end);
        $info->date = $this->date->format("Y-m-d");
        $info->weekday = $this->weekday;
        if( $user !== null ){
            if( $user->getType() === UserTypes::Student){
                $filter = array("student" => $user,
                                "lesson" => $this);
                $attendance = CollectionHelper::getInstance()
                    ->findByFilter("Attendance", $filter);
                if( is_array($attendance) && ! empty($attendance) ){
                    $info->inscribed = true;
                }
            }
            if( $user->getType() === UserTypes::Teacher ){
                $filter = array("teacher" => $user,
                                "lesson" => $this);
                $impartation = CollectionHelper::getInstance()
                    ->findByFilter("Impartation", $filter);
                if( is_array($impartation) && ! empty($impartation) ){
                    $info->due = true;
                }
            }
        }
        return json_decode(json_encode($info), true);
    }

    /**
     * @return mixed
     */
    public function getId() : int {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getClassroom() : Classroom {
        return $this->classroom;
    }

    /**
     * @return mixed
     */
    public function getTeacher() : Impartation {
        return $this->teacher;
    }

    /**
     * @return mixed
     */
    public function getSubject() : Subject {
        return $this->subject;
    }

    /**
     * @return mixed
     */
    public function getPeriodStart() : int
    {
        return $this->period_start;
    }

    /**
     * @return mixed
     */
    public function getShift()
    {
        return $this->shift;
    }

    /**
     * @return mixed
     */
    public function getWeekday()
    {
        return $this->weekday;
    }

    /**
     * @return mixed
     */
    public function getPeriodEnd()
    {
        return $this->period_end;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

}