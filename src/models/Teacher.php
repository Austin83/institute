<?php
/**
 * Created by PhpStorm.
 * User: agustin
 * Date: 24/03/19
 * Time: 05:52 PM
 */

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity @Table(name="teachers")
 **/
class Teacher extends User
{

    /**
     * @OneToMany(targetEntity="Lesson", mappedBy="teacher")
     */
    private $lessons;

    /**
     * @ManyToMany(targetEntity="Subject")
     * @JoinTable(name="teacher_subjects")
     */
    private $subjects;

    public function __construct( array $data){
        parent::__construct( $data );
        $this->lessons = new ArrayCollection();
        $this->subjects = new ArrayCollection();

        foreach ( $data['subjects'] as $id ){
            $subject = CollectionHelper::getInstance()->find('Subject', $id);
            if( $subject !== null ){
                $this->subjects->add( $subject );
            }
        }

        $GLOBALS['em']->persist($this);
        $GLOBALS['em']->flush();
    }

    public function getType() : int
    {
        return UserTypes::Teacher ;
    }

    public function canTeach( int $subject_id ) : int {
        foreach ($this->subjects as $subject){
            if( $subject_id === $subject->getId() ){
                return HttpStatus::OK;
            }
        }
        return HttpStatus::Not_Found;
    }

    public function getInfo( InfoHelper $filter_student ) : array
    {
        if( $filter_student === null || empty($filter_student->getFilter()) )
        {
            return $this->getInfoAll();
        }

        $info = new StdClass();

        switch ( $filter_student->getMode() )
        {
            case GetInfoModes::Optimistic:
                if( in_array(SelectTeacher::id,
                    $filter_student->getFilter()))
                {
                    $info->id = $this->id;
                }
                if( in_array( SelectTeacher::username,
                    $filter_student->getFilter()))
                {
                    $info->username = $this->username;
                }
                if( in_array( SelectTeacher::document,
                    $filter_student->getFilter()))
                {
                    $info->document = $this->document;
                }
                if( in_array( SelectTeacher::name,
                    $filter_student->getFilter()))
                {
                    $info->name = $this->name;
                }
                if( in_array( SelectTeacher::email,
                    $filter_student->getFilter()))
                {
                    $info->email = $this->email;
                }
                if( in_array( SelectTeacher::birthday,
                    $filter_student->getFilter()))
                {
                    $info->birthday = $this->birthday->format('d/m/Y');
                }
                if( in_array( SelectTeacher::registeredSince,
                    $filter_student->getFilter()))
                {
                    $info->registeredSince = $this
                        ->registeredSince->format('d/m/Y H:i:s');
                }
                if( in_array( SelectTeacher::phones,
                    $filter_student->getFilter()) )
                {
                    $info->phones = $this->phones;
                }
                if( in_array( SelectTeacher::fullname,
                    $filter_student->getFilter()) )
                {
                    $info->fullname = $this->name." ".$this->surname;
                }
                if( in_array( SelectTeacher::subjects,
                    $filter_student->getFilter()) )
                {
                    if( ! $this->subjects->isEmpty() ){
                        $info->subjects = array();
                        foreach ( $this->subjects as $sub ){
                            array_push($info->subjects,$sub->getId());
                        }
                    }

                }
                break;

            case GetInfoModes::Pesimistic:
                if( ! in_array(SelectTeacher::id,
                    $filter_student->getFilter()) )
                {
                    $info->id = $this->id;
                }
                if( ! in_array( SelectTeacher::username,
                    $filter_student->getFilter()) )
                {
                    $info->username = $this->username;
                }
                if( ! in_array( SelectTeacher::document,
                    $filter_student->getFilter()) )
                {
                    $info->document = $this->document;
                }
                if( ! in_array( SelectTeacher::name,
                    $filter_student->getFilter()) )
                {
                    $info->name = $this->name;
                }
                if( ! in_array( SelectTeacher::email,
                    $filter_student->getFilter()) )
                {
                    $info->email = $this->email;
                }
                if( ! in_array( SelectTeacher::birthday,
                    $filter_student->getFilter()) )
                {
                    $info->birthday = $this->birthday;
                }
                if( ! in_array( SelectTeacher::registeredSince,
                    $filter_student->getFilter()) )
                {
                    $info->registeredSince = $this
                        ->registeredSince;
                }
                if( ! in_array( SelectTeacher::phones,
                    $filter_student->getFilter()) )
                {
                    $info->phones = $this->phones;
                }
                if( ! in_array( SelectTeacher::fullname,
                    $filter_student->getFilter()) )
                {
                    $info->fullname = $this->name." ".$this->surname;
                }
                if( ! in_array( SelectTeacher::subjects,
                    $filter_student->getFilter()) )
                {
                    if( ! $this->subjects->isEmpty() ){
                        $info->subjects = array();
                        foreach ( $this->subjects as $sub ){
                            array_push($info->subjects,$sub->getId());
                        }
                    }

                }
                break;
        }
        return json_decode(json_encode($info), true);
    }

    public function getInfoAll() : array
    {
        $info = new StdClass();
        $info->type = $this->getType();
        $info->id = $this->id;
        $subjects = array();
        if( ! empty($this->getSubjects) ){
            foreach ($this->getSubjects as $subject){
                array_push($subjects, $subject->getId());
            }
        }
        $info->subjects = $subjects;
        $info->document = $this->document;
        $info->username = $this->username;
        $info->email = $this->email;
        $info->name = $this->name;
        $info->surname = $this->surname;
        $info->approved = $this->approved;
        $info->birthday = $this->birthday;
        $info->phones = $this->phones;
        $info->registeredSince = $this->registeredSince;
        return json_decode(json_encode($info), true);
    }

    public function updateByTeacher( array $data)
    {
        if( $data['password'] !== null ){
            $this->password = password_hash( $data['password'], PASSWORD_DEFAULT);
        }

        if( $data['email'] !== null){
            $this->email = $data['email'];
        }

        if( $data['add']['phones'] !== null ){
            $this->phones
                = array_unique(array_merge( $this->phones, $data['add']['phones']));
        }

        if( $data['remove']['phones'] !== null ){
            $this->phones =  \array_diff($this->phones, $data['remove']['phones']);
        }
    }

    public function updateByAdmin( array $data)
    {
        if( $data['user']['document'] !== null ){
            $this->document = $data['user']['document'];
        }

        if( $data['user']['password'] !== null ){
            $this->password = password_hash( $data['user']['password'], PASSWORD_DEFAULT);
        }

        if( $data['user']['name'] !== null){
            $this->name = $data['user']['name'];
        }

        if( $data['user']['email'] !== null){
            $this->email = $data['user']['email'];
        }

        if( $data['user']['birthday'] !== null ){
            $this->birthday = DateTime::createFromFormat('d/m/Y', $data['user']['birthday']);
        }

        if( $data['user']['registeredSince'] !== null ){
            $this->registeredSince =
                DateTime::createFromFormat('d/m/Y', $data['user']['registeredSince']);
        }

        if( $data['user']['add']['phones'] !== null ){
            $this->phones = array_unique(array_merge( $this->phones, $data['user']['add']['phones']));
        }

        if( $data['user']['remove']['phones'] !== null ){
            $this->phones =  \array_diff($this->phones, $data['user']['remove']['phones']);
        }
    }

    /**
     * @return Collection
     */
    public function getLessons(): Collection
    {
        return $this->lessons;
    }

    /**
     * @return mixed
     */
    public function getSubjects(): Collection
    {
        return $this->subjects;
    }
}