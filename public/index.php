<?php
/**
 * Created by PhpStorm.
 * User: sebastianndiaz
 * Date: 23/03/19
 * Time: 09:30 AM
 */

// Here's the front door. The real app begins with the next inclusion:
require_once dirname(__FILE__) . '/../bootstrap.php';