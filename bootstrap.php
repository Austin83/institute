<?php
/**
 * Created by PhpStorm.
 * User: sebastianndiaz
 * Date: 23/03/19
 * Time: 09:50 AM
 */

// Loading libraries (thanks, Composer)
require_once dirname(__FILE__) . '/vendor/autoload.php';

// Including namespaces
use \Slim\App;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// Loading Slim app's initial settings
$config = array();

$config['app'] = array(
  'name' => "Minerva API",
  'settings' => [
    'displayErrorDetails' => true
  ]
);

$app = new App($config['app']);

// Set headers for allowing the use of our API by external sources
header("Access-Control-Allow-Origin: *");

// And, also, set the default return type (JSON)
header("Content-Type: application/json; charset=UTF-8");

header("Access-Control-Allow-Headers: X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept, Authorization");

header("Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD");

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src/models"), $isDevMode);
$conn = array(
    'driver'   => 'pdo_mysql',
    'host'     => '127.0.0.1',
    'dbname'   => 'Institute',
    'user'     => 'Admin',
    'password' => '1234'
);

// Creating a global Entity Manager for our models
$GLOBALS['em'] = EntityManager::create($conn, $config);


$GLOBALS['tokenDuration'] = 7200;

// Including routes (one file for now; as it grows, we'll break it apart)
require_once dirname(__FILE__) . "/src/routes/routesPublic.php";
require_once dirname(__FILE__) . "/src/routes/routesUser.php";
require_once dirname(__FILE__) . "/src/routes/routesAdmin.php";

// Run Slim app
$app->run();