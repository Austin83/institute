<?php
require_once "bootstrap.php";
system('tput reset');
$object = new StdClass();
echo "Protocolo Minerva: Crear cuenta de Administrador\n";

$object->document = readline("Ingrese su documento : ");
while (strlen($object->document) > DataConstants::Document){
    echo "El documento ingresado supera los "
        .DataConstants::Document." cáracteres permitidos\n";
    $object->document = readline("Ingrese su documento : ");
}

$object->username = readline("Ingrese su nombre de usuario : ");
while (strlen($object->username) > DataConstants::Username){
    echo "El username ingresado supera los "
        .DataConstants::Username." cáracteres permitidos\n";
    $object->username = readline("Ingrese su nombre de usuario : ");
}

$object->password = readline("Ingrese su contraseña : ");
while (strlen($object->password) < DataConstants::Pass_min){
    echo "Su contraseña es muy corta, mínimo "
        .DataConstants::Pass_min." carácteres !\n";
    $object->password = readline("Ingrese su contraseña : ");
}
$object->email = readline("Ingrese su email : ");
$object->name = readline("Ingrese su nombre : ");
$object->surname = readline("Ingrese su apellido : ");
$object->birthday= readline("Ingrese su fecha de nacimiento (aaaa/mm/dd) : ");
$date = new DateTime("now", new DateTimeZone('America/Montevideo') );
$object
    ->registeredSince=$date->format('d/m/Y');
$object->phones = array();
array_push($object->phones, readline("Ingrese un teléfono o celular : "));
do {
    $condition = filter_var( readline("Ingresar un número más? 1-Yes 2-No : "),
        FILTER_VALIDATE_BOOLEAN);
    if(!$condition){
        break;
    }
    array_push($object->phones, readline("Ingrese un teléfono o celular : "));
}
while($condition);
$admin = null;
$data = json_decode(json_encode($object), true);
if (AdminsController::getInstance()->validateRegister($data))
{
    $admin = new Admin($data);
    $GLOBALS['em']->persist($admin);
    $GLOBALS['em']->flush();
    echo "Protocolo Minerva: cuenta creada con éxito\n";
}
else{
    echo "Protocolo Minerva: ERROR! revisar log para más detalles\n";
}

echo "Protocolo Minerva: Fin\n";