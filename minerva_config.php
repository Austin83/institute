<?php

    return [
        'serverName' => 'localhost',
        'jwtKey' => $fileContent = file_get_contents(getenv('HOME')."/.minerva/minerva.jwtkey"),
        'floors_top' => 3,
        'floors_under' => -1,
        'debug' => true
    ];
